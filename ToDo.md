ToDo for dictionary.lib

1. Tag
  a. tag_constants = const int name = tag;
  b. tag by type/class
      i.    tag.patient.name
      ii.   tag.study.name
      iii.  tag.series.name
      iv.   Tag.instance.name
      v.   Tag.class(tag) => 
      
2. VR

3. VM

4. Attribute - tag, vr, values

5. Dataset

6. Macros
    a. name, table_number, title, docLinks
    b. tag, type, condition, doc
    c. macro -> Attribute def list
    
7. Modules
    a. name, table number, title...
    b. ie_level
    c. tagList => tag, type, condition, doc
    d. macroList
    
8. IODs
    a. name, table number, title...
    b. modality
    c. tagList => tag, type, condition, doc
    d. macroList