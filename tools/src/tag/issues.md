Things to be done for next revision:

  1) Add an IELevel column to .csv file

  2) Add a special table generator routine for data_elements

  3) Make sure DICOS types are not removed from Retired column (maybe rename 
     the column to "Type")
  4) Make sure order is [keyword, tag, name, vr, vm, type, level]

Issues with Generating 'package:core/gen/tags.dart' from the PS3.6 Table 6-1

  1) Tags with multiple VRs: 
    OBOW
    USOW
    USSS
    USSSOW
  2) Tags with no VR
    NoVR
  3) Tags with no VM
    NoVM
  4) Tag with "," changed to ":" 

  5) Tags with "xx" in tag number

Design Issues:

  1) Does the Tags class need the [tagName]?  These could be stored in a separate library
  2) Should we use strings or integers for the lookup table keys?
  3) Should the lookup up table be a 1 or 2 level map?
