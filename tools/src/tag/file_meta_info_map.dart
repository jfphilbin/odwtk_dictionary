// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library file_meta_info_map;

Map fileMetaInfoMap =  {
  "JsonType":  "ClassTable",
  "className": "FileMetaInfo",
  "fields":    { "name": {"modifiers": "const",
                             "type": "String"},
                  "value": {"modifiers": "const",
                             "type": "int"},
                   "vr":   {"modifiers": "const",
                              "type": "VR"},
                   "vm": {"modifiers": "final",
                           "type": "VM"},
                   "isRetired":  {"modifiers": "final",
                                    "type": "bool"},
  "members":  [ {"value": "0x00020000",
                   "name":  "FileMetaInformationGroupLength",
                   "vr":     "UL",
                   "vm":    1,
                   "isRetired": false},
                   {"value": "0x00020001",
                    "name":  "FileMetaInformationVersion",
                    "vr":     "OB",
                    "vm":    1,
                    "isRetired": false},
                    {"value": "0x00020002",
                     "name":  "MediaStorageSOPClassUID",
                     "vr":     "OB",
                     "vm":    1,
                     "isRetired": false},
                     {"value": "0x00020001",
                      "name":  "FileMetaInformationVersion",
                      "vr":     "UI",
                      "vm":    1,
                      "isRetired": false},

                    /*
                       const [0x00020001, "FileMetaInformationVersion",      "OB", 1],

                       const [0x00020002, "MediaStorageSOPClassUID",         "UI", 1],
                       const [0x00020003, "MediaStorageSOPInstanceUID",      "UI", 1],
                       const [0x00020010, "TransferSyntaxUID",               "UI", 1],
                       const [0x00020012, "ImplementationClassUID",          "UI", 1],
                       const [0x00020013, "ImplementationVersionName",       "SH", 1],
                       const [0x00020016, "SourceApplicationEntityTitle",    "AE", 1],
                       const [0x00020017, "SendingApplicationEntityTitle",   "AE", 1],
                       const [0x00020018, "ReceivingApplicationEntityTitle", "AE", 1],
                       const [0x00020100, "PrivateInformationCreatorUID",    "UI", 1],
                       const [0x00020102, "PrivateInformation",              "OB", 1]
                       *
                        */
                     ]
};

