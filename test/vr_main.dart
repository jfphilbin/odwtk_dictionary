import 'dart:typed_data';

//import './vr.dart';

void main() {

  /*
  print('UL - AE = ${0x5554 - 0x4145}');
  print('U - A = ${0x55 -0x41}');
  print('W - A = ${0x57 - 0x41}');
  print('T - A = ${0x54 - 0x41}');
  print('20 x 22 = ${20 * 22}');
  print(vrByChar);

  //List topLevel = createVrLookupByChar(vrByChar);
  //print(topLevel);
*/
  const Uint8List foo = const [0, 1, 2, 3];
  print(foo[0]);
  //print(vrIndexLookup);
  //int i = lookupVRByChar(0x42, 0x52);
  //print('AE VR= $i');
}


const vrCodes = const [
    0x4145,
    0x4153,
    0x4154,
    0x4252,
    0x4353,
    0x4441,
    0x4453,
    0x4454,
    0x4644,
    0x464c,
    0x4953,
    0x4c4f,
    0x4c54,
    0x4f42,
    0x4f42,
    0x4f46,
    0x4f57,
    0x504e,
    0x5348,
    0x534c,
    0x5351,
    0x5353,
    0x5354,
    0x544d,
    0x5549,
    0x554c,
    0x554e,
    0x5552,
    0x5553,
    0x5554];


const List vrCodeMap = const [
    0x4145, 0x4153, 0x4154, 0x4252, 0x4353, 0x4441, 0x4453, 0x4454, 0x4644, 0x464c,
    0x4953, 0x4c4f, 0x4c54, 0x4f42, 0x4f44, 0x4f46, 0x4f57, 0x504e, 0x5348, 0x534c,
    0x5351, 0x5353, 0x5354, 0x544d, 0x5549, 0x554c, 0x554e, 0x5552, 0x5553, 0x5554];

List<int> lookupFirstChar = new Uint8List(20);
/*
Uint8List<int> AList = new Uint8List(19);
Uint8List<int> BList = new Uint8List(17);
Uint8List<int> CList = new Uint8List(17);
Uint8List<int> DList = new Uint8List(17);
Uint8List<int> FList = new Uint8List(17);
Uint8List<int> IList = new Uint8List(17);
Uint8List<int> LList = new Uint8List(17);
Uint8List<int> OList = new Uint8List(17);
Uint8List<int> PList = new Uint8List(17);
Uint8List<int> SList = new Uint8List(17);
Uint8List<int> TList = new Uint8List(17);
Uint8List<int> UList = new Uint8List(17);

 */

int strCharToInt(String sChar) => sChar.codeUnitAt(0) - 0x41;
int ucCharToInt(int char) => char - 0x41;

const Map<String, int> nameToIndex =
    const {"AE": 00, "AS": 01, "AT": 02, "BR": 03, "CS": 04, "DA": 05,
           "DS": 06, "DT": 07, "FD": 08, "FL": 09, "IS": 10, "LO": 11,
           "LT": 12, "OB": 13, "OD": 14, "OF": 15, "OW": 16, "PN": 17,
           "SH": 18, "SL": 19, "SQ": 20, "SS": 21, "ST": 22, "TM": 23,
           "UI": 24, "UL": 25, "UN": 26, "UR": 27, "US": 28, "UT": 29};


List createVrLookupByChar(List top) {
  List<Uint8List> topLevel = new List(0x56 - 0x41);

  for(int i = 0; i < top.length; i++) {
    List l = top[i];
    int index = strCharToInt(l[0]);
    print('index=$index');
    int    length = l[1];
    print('length=$length');
    List elts = l[2];
    print('elts=$elts');
    Uint8List currentUint8List = new Uint8List(length);
    topLevel[index] = currentUint8List;
    for(int j = 0; j < elts.length; j++) {
      List vrs = elts[j];
      int offset = strCharToInt(vrs[0]);
      int vrIndex = nameToIndex[vrs[2]];
      currentUint8List[offset] = vrIndex;
      print('name=${vrs[2]}, index=$vrIndex');
    }
  }
  return topLevel;
}
List vrByChar =  [
 ['A', 0x55 - 0x41, [['E', 0x4145, "AE"], ['S', 0x4153, "AS"],['T', 0x4154, "AT"]]],
 ['B', 0x53 - 0x41, [['R', 0x4252, "BR"]]],
 ['C', 0x54 - 0x41, [['S', 0x4353, "CS"]]],
 ['D', 0x55 - 0x41, [['A', 0x4441, "DA"], ['S', 0x4453, "DS"], ['T', 0x4454, "DT"]]],
 ['F', 0x4d - 0x41, [['D', 0x4644, "FD"], ["L", 0x464c, "FL"]]],
 ['I', 0x54 - 0x41, [['S', 0x4953, "IS"]]],
 ['L', 0x55 - 0x41, [['O', 0x4c4f, "LO"], ['T', 0x4c54, "LT"]]],
 ['O', 0x58 - 0x41, [['B', 0x4f42, "OB"], ['D', 0x4f42, "OD"], ['F', 0x4f46, "OF"], ['W', 0x4f57, "OW"]]],
 ['P', 0x4f - 0x41, [['N', 0x504e, "PN"]]],
 ['S', 0x55 - 0x41, [['H', 0x5348, "SH"], ['L', 0x534c, "SL"], ['Q', 0x5351, "SQ"], ['S', 0x5353, "SS"], ['T', 0x5354, "ST"]]],
 ['T', 0x4f - 0x41, [['M', 0x544d, "TM"]]],
 ['U', 0x55 - 0x41, [['I', 0x5549, "UI"], ['L', 0x554c, "UL"], ['N', 0x554e, "UN"], ['R', 0x5552, "UR"], ['S', 0x5553, "US"], ['T', 0x5554, "UT"]]]
 ];

const List A = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2];
const List B = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3];
const List C = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4];
const List D = const [5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 7];
const List F = const [0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 9];
const List I = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10];
const List L = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 12];
const List O = const [0, 13, 0, 14, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16];
const List P = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17];
const List S = const [0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 19, 0, 0, 0, 0, 20, 0, 21, 22];
const List T = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0];
const List U = const [0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 25, 0, 26, 0, 0, 0, 27, 28, 29];

const List<List<int>> vrIndexLookup =
    const [A, B, C, D, null, F, null, null, I, null, null, L, null, null,
           O, P, null, null, S, T, U, null, null, null, null];
