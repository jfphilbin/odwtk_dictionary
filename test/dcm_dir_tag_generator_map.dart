// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dcm_dir_tag_generator_map;

Map dcmDirTagGeneratorMap = {
   // The Type of this map
   "MapType": "Constant Class for DICOM Dir Tags",
   // The version number of this map should be updated when edited
   "version": "0.1.1",
   // The name of the class
  "name": "DcmDirTag",
  // The filename of the generated file
  "filename": "dcm_dir_tag",
  // DocGen comment describing the class
  "description": """
 /**
 *  DICOM Director Tag Class
 *
 *  See [PS 3.6](http://medical.nema.org/MEDICAL/dicom/2014a/output/pdf/part06.pdf)
 */
 """,
  // All fields are 'final'
  "fields": [{
      "type": "int",
      "name": "tag"
    }, {
      "type": "String",
      "name": "keyword"
    }, {
      "type": "VR",
      "name": "vr"
    }, {
      "type": "VM",
      "name": "vm"
    }, {
      "type": "bool",
      "name": "retired"
    }],
  "members": [
      ["0x00041130", "FileSetID", "VR.CS", "VM.VM_1", false],
      ["0x00041141", "FileSetDescriptorFileID", "VR.CS", "VM.VM_1_8", false],
      ["0x00041142", "SpecificCharacterSetOfFileSetDescriptorFile", "VR.CS", "VM.VM_1", false],
      ["0x00041200", "OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity", "VR.UL", "VM.VM_1", false],
      ["0x00041202", "OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity", "VR.UL", "VM.VM_1", false],
      ["0x00041212", "FileSetConsistencyFlag", "VR.US", "VM.VM_1", false],
      ["0x00041220", "DirectoryRecordSequence", "VR.SQ", "VM.VM_1", false],
      ["0x00041400", "OffsetOfTheNextDirectoryRecord", "VR.UL", "VM.VM_1", false],
      ["0x00041410", "RecordInUseFlag", "VR.US", "VM.VM_1", false],
      ["0x00041420", "OffsetOfReferencedLowerLevelDirectoryEntity", "VR.UL", "VM.VM_1", false],
      ["0x00041430", "DirectoryRecordType", "VR.CS", "VM.VM_1", false],
      ["0x00041432", "PrivateRecordUID", "VR.UI", "VM.VM_1", false],
      ["0x00041500", "ReferencedFileID" "VR.CS", "VM.VM_1_8", false],
      ["0x00041504", "MRDRDirectoryRecordOffset", "VR.UL", "VM.VM_1", true],
      ["0x00041510", "ReferencedSOPClassUIDInFile", "VR.UI", "VM.VM_1", false],
      ["0x00041511", "ReferencedSOPInstanceUIDInFile", "VR.UI", "VM.VM_1", false],
      ["0x00041512", "ReferencedTransferSyntaxUIDInFile", "VR.UI", "VM.VM_1", false],
      ["0x0004151A", "ReferencedRelatedGeneralSOPClassUIDInFile", "VR.UI", "VM.VM_1_n", false],
      ["0x00041600", "NumberOfReferences", "VR.UL", "VM.VM_1", true]]
};
