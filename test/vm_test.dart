// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>


import 'package:unittest/unittest.dart';

import 'package:dictionary/vm.dart';

void main() {
  /*
  print(VM.VM_1);
  print(VM.lookup("VM_1"));
  print(VM.VM_1.validate([1]));
  print(VM.VM_3_3n);
  print(VM.lookup("VM_3_3n"));
  print(VM.VM_3_3n.validate([1,2,3,4,5,6]));
 */
  test('Basic VM Test', () {
    expect(VM.lookup("VM_1"), equals(VM.VM_1));
    expect(VM.lookup("VM_3_3N"), equals(VM.VM_3_3n));
  });
}