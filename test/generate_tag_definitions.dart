// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

/**
 * Program used to generate DICOM Character Class Predicates for Validating Value Field values.
 */
import 'dart:io';
import 'package:dictionary/src/tag/keyword_to_tag_map.dart';

const String jsonFilename   = 'tag_table.json';
const String outputDir      = './test_output/';
const String outputFilename = 'tag_definitions.dart';
const String generator      = 'generate_tag_constants.dart';

int parseError(String s) {
  print('parseError($s)');
  return -1;
}

void main() {
  print('Tag Definitions');
  print(keywordToTag);

  List pairs = new List();
  List problems = new List();
  keywordToTag.forEach((keyword, tag) {
   int n = int.parse(tag, radix: 16, onError: parseError);
   if (n == -1) problems.add([tag, keyword]);
   else pairs.add([n, keyword]);
  });

  compare(List a, List b) {
    var x = a[0];
    var y = b[0];
    return x.compareTo(y);
  }
  pairs.sort(compare);
  //print(pairs);
  File file = new File('./tag_definitions.dart');
  String source = """
// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library tag_definitions;


/*****************************************************************
 *****  This file was generated from generate_tag_definitions.dart
 *****************************************************************/

/**
 * [keyword] = [tag] definitions
 *
 * Compile-Time constant definitionas for Standard DICOM Data Element [Keywords]
 * with a value equal to the [int] value of their tag.
 */
""";

  for (List l in pairs) {
    String key = l[1];
    String tag = l[0].toRadixString(16);
    tag.padLeft(8);
    print('int $key =  0x$tag;');
    source += 'const int \$$key =  0x$tag;\n';
  }
  source += """

/**
 * Problem Data Elements: Their tag values cannot be integers.
 */
""";

  problems.sort(compare);
 // print(problems);
  for (List l in problems) {
    String key = l[1];
    String tag = l[0];
    print('String $key =  "0x$tag";');
    source += 'const String \$$key =  "0x$tag";\n';
    file.writeAsStringSync(source);
  }
}





