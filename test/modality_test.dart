// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:unittest/unittest.dart';
import 'package:dictionary/modality.dart';

void main() {
  testModality();
}

void testModality() {
  print("$Modality.DS");
  print("'${Modality.DS.name}'");
  test('Modality.CT', () => expect(Modality.CT.name, "Computed Tomography"));
  test('Modality.DS', () => expect(Modality.lookup(#D), Modality.XA));

}
