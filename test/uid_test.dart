// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:dictionary/uid.dart';
//import 'package:core/dcm_uuid.dart';

List validUIDs = [
                  UID.VerificationSOPClass,
                  UID.ImplicitVRLittleEndianDefaultTransferSyntaxforDICOM,
                  UID.ExplicitVRLittleEndian,
                  UID.DeflatedExplicitVRLittleEndian,
                  UID.ExplicitVRBigEndian_Retired,
                  UID.JPEGBaseline_1DefaultTransferSyntaxforLossyJPEG8BitImageCompression,
                  UID.JPEGExtended_2_4DefaultTransferSyntaxforLossyJPEG12BitImageCompression_4,
                  UID.JPEGExtended_3_5_Retired,
                  UID.JPEGSpectralSelectionNon_Hierarchical_6_8_Retired,
                  UID.JPEGSpectralSelectionNon_Hierarchical_7_9_Retired,
                  UID.JPEGFullProgressionNon_Hierarchical_10_12_Retired,
                  /*
                  UID.VerificationSOPClass,
                  UID.ImplicitVRLittleEndian,
                  UID.ExplicitVRLittleEndian,
                  UID.DeflatedExplicitVRLittleEndian,
                  UID.ExplicitVRBigEndian,
                  UID.JPEGBaseline1,
                  UID.JPEGExtended24,
                  UID.JPEGExtended35Retired,
                  */
                  new UID("1.2.0"),
                  new UID("1.2.840.113696.596650.500.6992491.20131003190152"),
                  new UID("1.2.840.113696.596650.500.7008750.20131008131113"),
                  new UID("1.2.826.0.1.3680043.2.93.9.2.87494905237.2000955811"),
                  new UID("1.2.840.113619.2.290.3.3233817346.757.1381538330.742"),
                  new UID("1.2.840.113619.20131019.12578121"),
                  new UID("1.2.840.113696.596650.500.7060209.20131022083918"),
                  new UID("1.2.840.113696.596650.500.7060197.2013102208325"),
                  new UID("1.2.840.113696.596650.500.7023315.20131011135827"),
                  new UID("1.2.840.113696.596650.500.7055589.20131021090226")
                  ];

List invalidUIDs =
    [ new UID("4.23"),
      new UID("1.023"),
      new UID("1.00.3"),
      new UID("1.23.01"),
      new UID("0.1.2.0456"),
      new UID("302623232524212121262130"),
      new UID("2.00.242123292124212226232830"),
      new UID("1.2.840.10008.20.13.97.109.48.53.524320.11421080.527076.1432000001"),
      new UID("3900632239.632463993267187500.203179425.635168500943401758"),
      new UID("4.3900632239.632463993267187500.203179425.635168402307483104"),
      new UID("1.2.00.3900632239.632463993267187500"),
      new UID("242122292124212128212422"),
      new UID("65.2.840.10008.20.13.97.98.48.49.954.3223026.538059.3571"),
      new UID("262122302624212128222625"),
      new UID("302123212124212222232522"),
      new UID("65.2.840.10008.20.13.97.105.48.51.954.3223026.538059.3571"),
      new UID("120098248412128687095293133475624581165"),
      new UID("242123292124212226232830"),
      new UID("65.2.840.10008.20.13.97.109.48.53.524320.11421080.527076.1432"),
      new UID("3900632239.632463993267187500.203179425.635180539252155650"),
      new UID("302122262124212229242924"),
      new UID("3900632239.632463993267187500.203179425.635188161169143126")
    ];

void checkUID(UID uid) {
  bool valid = UID.isValid(uid);
  int n = uid.uid.length;
  print("$n: $valid for $uid");
}

void main() {

  //TODO write unit test for uid.dart

  UID uid = UID.randomUID();
  checkUID(uid);

  for (UID uid in validUIDs) {
    checkUID(uid);
  }

  for (UID uid in invalidUIDs) {
    checkUID(uid);
  }

}

