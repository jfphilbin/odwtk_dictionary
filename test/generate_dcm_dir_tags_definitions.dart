// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

Map dcmDirTagMap = {
  "FileSetID":                                     "0x00041130",
  "FileSetDescriptorFileID":                       "0x00041141",
  "OffsetOfTheFirstDirectoryRecord":               "0x00041142",
  "OffsetOfTheLastDirectoryRecord":                "0x00041200",
  "OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity": "0x00041202",
  "FileSetConsistencyFlag":                        "0x00041212",
  "DirectoryRecordSequence":                       "0x00041220",
  "OffsetOfTheNextDirectoryRecord":                "0x00041400",
  "RecordInUseFlag":                               "0x00041410",
  "OffsetOfReferencedLowerLevelDirectoryEntity":   "0x00041420",
  "DirectoryRecordType":                           "0x00041430",
  "PrivateRecordUID":                              "0x00041432",
  "ReferencedFileID":                              "0x00041500",
  "MRDRDirectoryRecordOffset":                     "0x00041504",
  "ReferencedSOPClassUIDInFile":                   "0x00041510",
  "ReferencedSOPInstanceUIDInFile":                "0x00041511",
  "ReferencedTransferSyntaxUIDInFile":             "0x00041512",
  "ReferencedRelatedGeneralSOPClassUIDInFile":     "0x0004151A",
  "NumberOfReferences":                            "0x00041600",
};

String getProjectHeader() {
  return """
// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
""";
}
String getClassStart(String source, Map map) {
  return """

library ${map["fileName"]};

${map["description"]}
/**
 *  DICOM Director Tag Class
 *
 *  See [PS 3.6](http://medical.nema.org/MEDICAL/dicom/2014a/output/pdf/part06.pdf)
 */

class ${map["name"]} {
${map["fieldDefs"]}
  final int    tag;
  final String keyword;
  final VR     vr;
  final VM     vm;

${map["constructor"]}
  const DcmDirTag(this.tag, this.keyword, this.vr, this.vm);
${map["members"]}

}
""";
}

String generateMembers(String source, Map map) {
  for (List m in map["members"]) {
    source += '  static const ${m[0]} = const $map["name"](${m[1]}, ${m[0]}, ${m[2]}, ${m[3]});';
  }
  return source;
}