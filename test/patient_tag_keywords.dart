// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
//library patient_tag_keywords;

/**
 *  A Compile Time Constant Map of patient level Data Element [keywords] and
 *  associate [tag] values a [String]s.
 */

//TODO Make sure the mappings in the table are accurate.
const Map<String, String> patientTagKeywordToValue = const {
  "ReferencedPatientSequence": "00081120",

  //"PatientGroupLength": "00100000",
  "PatientName": "00100010",
  "PatientID": "00100020",
  "IssuerOfPatientID": "00100021",
  "IssuerOfPatientIDQualifiersSequence": "00100024",
       // These are pat of the sequence id
      "UniversalEntityID":  "00400032",
      "UniversalEntityIDType": "00400033",
      "IdentifierTypeCode": "00400035",
      "AssigningFacilitySequence":   "00400036",
      "AssigningJurisdictionCodeSequence":  "00400039",
      "AssigningAgencyOrDepartmentCodeSequence":  "0040003A",

  "QualityControlSubject": "00100200",
  "OtherPatientIDs": "00101000",
  "OtherPatientNames": "00101001",
  "OtherPatientIDsSequence": "00101002",

      // contained in sequence
      /*
      "PatientID": "00100020",
      "IssuerOfPatientID": "00100021",
      "IssuerOfPatientIDQualifiersSequence": "00100024",
      "TypeOfPatientID": "00100022",
      */

  "TypeOfPatientID": "00100022",
  "PatientBirthDate": "00100030",
  "PatientBirthTime": "00100032",
  "PatientSex": "00100040",
  "PatientInsurancePlanCodeSequence": "00100050",
  "PatientPrimaryLanguageCodeSeq": "00100101",
  "PatientPrimaryLanguageCodeModSeq": "00100102",

  "PatientBirthName": "00101005",
  "PatientAge": "00101010",
  "PatientSize": "00101020",
  "PatientWeight": "00101030",
  "PatientAddress": "00101040",
  "InsurancePlanIdentification": "00101050",
  "PatientMotherBirthName": "00101060",
  "MilitaryRank": "00101080",
  "BranchOfService": "00101081",
  "MedicalRecordLocator": "00101090",
  "ReferencedPatientPhotoSequence": "00101100",
  "MedicalAlerts": "00102000",
  "Allergies": "00102110",
  "CountryOfResidence": "00102150",
  "RegionOfResidence": "00102152",
  "PatientTelephoneNumbers": "00102154",
  "EthnicGroup": "00102160",
  "Occupation": "00102180",
  "SmokingStatus": "001021A0",
  "AdditionalPatientHistory": "001021B0",
  "PregnancyStatus": "001021C0",
  "LastMenstrualDate": "001021D0",
  "PatientReligiousPreference": "001021F0",
  "PatientSpeciesDescription": "00102201",
  "PatientSpeciesCodeSequence": "00102202",
  "PatientSexNeutered": "00102203",
  "PatientBreedDescription": "00102292",
  "PatientBreedCodeSequence": "00102293",
  "BreedRegistrationSequence": "00102294",
  "BreedRegistrationNumber": "00102295",
  "BreedRegistryCodeSequence": "00102296",
  "ResponsiblePerson": "00102297",
  "ResponsiblePersonRole": "00102298",
  "ResponsibleOrganization": "00102299",
  "PatientComments": "00104000",

  // Trial info
  /* don't include
  "ClinicalTrialSponsorName": "00120010",
  "ClinicalTrialProtocolID": "00120020",
  "ClinicalTrialProtocolName": "00120021",
  "ClinicalTrialSiteID": "00120030",
  "ClinicalTrialSiteName": "00120031",
  "ClinicalTrialSubjectID": "00120040",
  "ClinicalTrialSubjectReadingID": "00120042",
  */
  "PatientIdentityRemoved": "00120062",
  "DeidentificationMethod": "00120063",
  "DeidentificationMethodCodeSequence": "00120064",

  // clinical trial attributes not included
  /*
  "ClinicalTrialSupbectReadingID": "00120081",
  "ClinicalTrialProtocolEthicsCommitteeName": "00120081",
  "ClinicalTrialEthicsCommitteeApprovalNumber": "00120082",
  */



  // Person Identification Macro Attributes
  /* don't include
  "PersonIdentificationCodeSequence": "00401101",
  "PersonAddress": "00401102",
  "PersonTelephoneNumbers": "00401103",
  "InstitutionName": "00080080",
  "InstitutionCodeSequence": "00080082",
  "InstitutionAddress": "00080081",
  */
};

void main() {

  List keywords = getKeys(patientTagKeywordToValue);
  print("keywords=$keywords");
  sort(keywords);
  print("keywords=$keywords");

  List tags = getValues(patientTagKeywordToValue);
  print("Tags=$tags");
  sort(tags);
  print("Tags=$tags");

  Map map = new Map();
  for (int i = 0; i < tags.length; i++) map[tags[i]] = keywords[i];
  printMap(map);
  printSwappedMap(map);

  List tagInt = new List<int>();
  for (String tag in tags) {
    int i = int.parse(tag, radix: 16);
    tagInt.add(i);
  }
  //Map<String, String>spm = new SplayTreeMap.from(patientTagKeywordToValue);
  List sortedKeys = getKeys(patientTagKeywordToValue);
  print("sortedKeys=$sortedKeys");
  List sortedTags = getValues(patientTagKeywordToValue);
  print("sortedTagss=$sortedTags");


}

List getKeys(Map map) {
  List list = new List();
  for (String key in map.keys) {
    list.add(key);
  }
  return list;
}

List getValues(Map map) {
  List list = new List();
  for (String key in map.values) {
    list.add(key);
  }
  return list;
}

sort(List l) {
  //if (l is List<String>) l.sort((String a, String b) => a.compareTo(b));
  //if (l is List<int>) l.sort((int a, int b) => a.compareTo(b));
 l.sort((String a, String b) => a.compareTo(b));
}

printMap(Map m) {
  print('Map<int, String patientTagKeywordMap = {');
  m.forEach((k, v) {print('  0x$k: "$v",'); });
  print('  };');
}

printSwappedMap(Map m) {
  print('Map patientTagNameMap = {');
  m.forEach((k, v) { print('  "$v": 0x$k,'); });
  print('  };');
}

