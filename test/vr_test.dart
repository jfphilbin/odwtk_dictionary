// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

//import 'dart:typed_data';
import 'package:unittest/unittest.dart';

import 'package:dictionary/vr.dart';

void main() {

  //TODO is this needed?  If so, where should it be done?

  group('VR Tests', () {

    test('VR.AE', () => expect(VR.codeToVR(0x4145), VR.AE));
    test('VR.UT', () => expect(VR.codeToVR(0x5554), VR.UT));

    test('VR.UT.vfSize', () => expect(VR.codeToVR(0x5554).vfSize, equals(4)));

    test("AE", () {
      expect(VR.stringToVR('AE'), equals(VR.AE));
      //expect(VR.valueOf('AE'), equals(VR.AE));

    });

  });

}
