// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:more/collection.dart';
import 'package:unittest/unittest.dart';
import 'package:utilities/ascii.dart';
import 'package:dictionary/dictionary.dart';

//import 'package:utilities/utilities.dart';


void main() {

  bool testBitInSet(BitList charset, int offset) {
    return charset[offset] == true;
  }

  bool testBitNotInSet(BitList charset, int offset) {
    return charset[offset] == false;
  }

  bool testBitRangeInSet(BitList charset, start, end) {
    for(var i = start; i < end; i++) {
      if (charset[i] == false) throw new Error();
    }
    return true;
  }

  bool testBitRangeNotInSet(BitList charset, start, end) {
    for(var i = start; i < end; i++) {
      if (charset[i] == true) throw new Error();
    }
    return true;
  }

  bool testDigits(BitList charset) {
    return testBitRangeInSet(charset, $DIGIT_0, $DIGIT_9+1);
  }

  group('ascii_constants', () {

    test('AETitle', () {
      BitList charset = $AETITLE;

      // SPACE to TILDE - [BACKSLASH]
      testBitRangeNotInSet(charset, 0, $SPACE);
      testBitRangeInSet(charset, $SPACE, $BACKSLASH);
      testBitNotInSet(charset, $BACKSLASH);
      testBitRangeInSet(charset, $BACKSLASH+1, $TILDE+1);
      testBitNotInSet(charset, 127);
    });

    solo_test('Age', () {
      BitList charset = $AETITLE;

      // SPACE to TILDE - [BACKSLASH]
      testBitRangeNotInSet(charset, 0, $DIGIT_0);
      testDigits(charset);
      testBitRangeNotInSet(charset, $DIGIT_9+1, $D);
      testBitInSet(charset, $D);
      testBitRangeNotInSet(charset, $D+1 , $M);
      testBitInSet(charset, $M);
      testBitRangeNotInSet(charset, $M+1 , $W);
      testBitInSet(charset, $W);
      testBitRangeNotInSet(charset, $W+1 , $Y);
      testBitInSet(charset, $Y);
      testBitRangeInSet(charset, $Y+1, 127);
    });

  });
}