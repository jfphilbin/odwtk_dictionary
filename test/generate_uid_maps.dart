// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:dictionary/uid.dart';

void main() {

  //print(UID_LIST[0]);
  //String keyword = nameToKeyword(UID_LIST[0].name);
 // print('keyword=$keyword');
  List<UID> uidList = $UIDDefsList.sublist(0);

 // print('old len = ${UID_LIST.length}, new len = ${uidList.length}');

  uidList.sort(uidNameCompare);

 // String out = generateKeywordToUIDMap("");
 // String out = generateUIDToKeywordMap("");
 // String out = generateKeywordToUIDDefMap("");
  String out = generateUIDToUIDDefMap("");

  print(out);
}

uidNameCompare(UID u1, UID u2) => u1.name.compareTo(u2.name);

String nameToKeyword(String name) {
  var keyword = "";
  for(int i = 0; i < name.length; i++) {
    var s = name[i];
    if (s != " ") keyword += s;
  }
  return keyword;
}

String generateKeywordToUIDMap(String out) {
  String s = """
library keyword_to_uid_map;

const Map<String, String> keywordToUIDMap = const {
""";
  for(int i = 0; i < keywordToUIDMap.length; i++) {
    String keyword = keywordToUIDMap[i];
    String uid     = $UIDDefsList[i].uid;
    s += '"$keyword": "$uid",\n';
  }
  s += "};";
  return out + s;
}

String generateKeywordToUIDDefMap(String out) {
  String s = """
library keyword_to_uid_def_map;

import 'package:dictionary/uid.dart';

const Map<String, String> \$KeywordToUIDDefMap = const {
""";
  for(String keyword in keywordToUIDMap) {
    s += '"$keyword": UID.$keyword,\n';
  }
  s += "};";
  return out + s;
}

String generateUIDToKeywordMap(String out) {
  String s = """
library uid_to_keyword_map;

const Map<String, String> \$UIDToKeywordMap = const  {

""";
  for(UID uid in $UIDDefsList) {
    var keyword = nameToKeyword(uid.name);
    s += '"${uid.uid}": "$keyword",\n';
  }
  s += "};";
  return out + s;
}

String generateUIDToUIDDefMap(String out) {
  String s = """
library uid_to_uid_def_map;

import 'package:dictionary/uid.dart';

const Map<String, String> \$UIDToUIDDefMap = const  {
""";
  for(int i = 0; i < keywordToUIDMap.length; i++) {
    String keyword = keywordToUIDMap[i];
    String uid     = $UIDDefsList[i].uid;
    s += '"$uid": UID.$keyword,\n';
  }
  s += "};";
  return out + s;
}