// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

// Program: string_utils_test;

import 'package:logging/logging.dart';
import 'package:dictionary/tag.dart';
import 'package:utilities/utilities.dart';


void main() {

  startLogging('logs/string_utils_test.log', hierarchical: true, level: Level.INFO);
  final log = new Logger("string_util_test");

  List<int> ints = [0x00021111, 0x11112222, 0x22223333, 0xFFFE0000];

  log.info(intToHex(ints[1]));
  log.info(tagToHex(ints[2]));
  log.info(tagToDcmFmt(ints[3]));

  log.info(intListToHex(ints));
  log.info(tagListToHex(ints));
  log.info(tagListToDicom(ints));

  log.info(hexListToString(intListToHex(ints)));
  log.info(hexListToString(tagListToHex(ints)));
  log.info(hexListToString(tagListToDicom(ints)));

  var bar = intListToString(tagToDcmFmt);

  log.info('bar=${bar(ints)}');

}