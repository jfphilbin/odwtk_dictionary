// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

/**
 * A set of functions to generate compile time constant class definitions.
 */
//TODO finish this.
String getProjectHeader = """
// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
""";

String getClassStart(String source, Map map) {
  return """

library ${map["fileName"]};

${map["description"]}
/**
 *  DICOM Director Tag Class
 *
 *  See [PS 3.6](http://medical.nema.org/MEDICAL/dicom/2014a/output/pdf/part06.pdf)
 */

class ${map["name"]} {
${getFields(map)}

${getConstructor(map)}

${getMembers(source, map)}

}
""";
}

String getFields(Map map) {
  String s = "";
  for (Map field in map["fields"]) {
    s += "  final ${field["type"]} ${field["name"]};";
  }
  return s;
}

String getConstructor(map) {
  String s = "  const ${map["name"]}(";
  for (Map field in map["fields"]) {
    s += 'thsi.${field["name"]}, ';
  }
  return s;
}

String getMembers(String source, Map map) {
  for (List m in map["members"]) {

    source += '  static const ${m[1]} = const DcmDirTag(${m[0]}, ${m[1]}, ${m[2]}, ${m[3]});';
  }
  return source;
}