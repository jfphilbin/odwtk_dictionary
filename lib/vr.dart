// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library vr;

import 'dart:convert';

import 'package:dictionary/dicom_charsets.dart';

typedef bool charsetPredicate(int);

/**
 * DICOM Value Representations
 */
class VR {
  // Fields of members;
  final String symbol;         //TODO should this be a String or Symbol?
  final int    code;           // The integer encoding of the VR
  final int    vfSize;         // The number of bytes in the Length of Value Field
  final int    paddingByte;    // Either ASCII null (0) or space (32)
  final int    maxLength;      // The maximum length of a single value
  final bool   multipleValues; // Can the Value Field contain multiple values
  final bool   inlineBinary;   // Can this VR be inlined
  final charsetPredicate  charset;        // BitList of length 128 with legal chars being set true
  final String trimWhitespace; // Trim leading and trailing or just trailing
  final String name;

  const VR(this.symbol, this.code, this.vfSize, this.paddingByte, this.maxLength,
           this.multipleValues, this.inlineBinary, this.charset, this.trimWhitespace,
           this.name);

  // In the table below 32 = ' ' and 0 = ascii null
  //          Symbol        String  Code Size Pad Len    MV     InLine Charset Whitspce Name
  static const AE = const VR('AE', 0x4145, 2, 32, 16,    true,  false, isAETitleChar,    "trim",     "AETitle");
  static const AS = const VR('AS', 0x4153, 2, 32, 4,     false, false, null,       null,       "Age");
  static const AT = const VR('AT', 0x4154, 2,  0, 4,     true,  false, null,       null,       "Tag");
  static const BR = const VR('BR', 0x4252, 2,  0, -1,    true,  true,  null,       null,       "BDReference");
  static const CS = const VR('CS', 0x4353, 2, 32, 16,    true,  false, isCodeStringChar, "trim",     "CodeString");
  static const DA = const VR('DA', 0x4441, 2, 32, 8,     true,  false, isDateChar,       null,       "Date");
  static const DS = const VR('DS', 0x4453, 2, 32, 16,    true,  false, isDecimalChar,    "trim",     "Decimal");
  static const DT = const VR('DT', 0x4454, 2, 32, 26,    true,  false, isDateTimeChar,   "trailing", "DataTime");
  static const FD = const VR('FD', 0x4644, 2,  0, 8,     true,  false, null,       null,       "Float64");
  static const FL = const VR('FL', 0x464c, 2,  0, 4,     true,  false, null,       null,       "Float32");
  static const IS = const VR('IS', 0x4953, 2, 32, 12,    true,  false, isIntegerChar,    "trim",     "Integer");
  static const LO = const VR('LO', 0x4c4f, 2, 32, 64,    true,  false, isStringChar,     "trim",     "LongString");
  static const LT = const VR('LT', 0x4c54, 2, 32, 10240, false, false, isTextChar,       "trailing", "LongText");
  static const OB = const VR('OB', 0x4f42, 4,  0, 1,     false, true,  null,       null,       "OtherByte");
  static const OD = const VR('OD', 0x4f44, 4,  0, 8,     false, true,  null,       null,       "OtherDouble");
  static const OF = const VR('OF', 0x4f46, 4,  0, 4,     false, true,  null,       null,       "OtherFloat");
  static const OW = const VR('OW', 0x4f57, 4,  0, 2,     false, true,  null,       null,       "OtherWord");
  static const PN = const VR('PN', 0x504e, 2, 32, 64,    true,  false, isPersonNameChar, "trim",     "PersonName");
  static const SH = const VR('SH', 0x5348, 2, 32, 16,    true,  false, isStringChar,     "trim",     "ShortString");
  static const SL = const VR('SL', 0x534c, 2,  0, 4,     true,  false, null,       null,       "Int32");
  static const SQ = const VR('SQ', 0x5351, 4,  0, -1,    false,  false, null,       null,       "Sequence");
  static const SS = const VR('SS', 0x5353, 2,  0, 2,     true,  false, null,       null,       "Int16");
  static const ST = const VR('ST', 0x5354, 2, 32, 1024,  false, false, isTextChar,       "trailing", "ShortText");
  static const TM = const VR('TM', 0x544d, 2, 32, 16,    true,  false, isTimeChar,   "trailing", "Time");
  static const UI = const VR('UI', 0x5549, 2,  0, 64,    true,  false, null,       null,       "Uid");
  static const UL = const VR('UL', 0x554c, 2,  0, 4,     true,  false, null,       null,       "Uint32");
  static const UN = const VR('UN', 0x554e, 4,  0, -1,    true,  true,  null,       null,       "Unknown");
  static const UR = const VR('UR', 0x5552, 4, 32, -1,    false, false, isURIChar,        "trim",     "URI");
  static const US = const VR('US', 0x5553, 2,  0, 2,     true,  false, null,       null,       "Uint16");
  static const UT = const VR('UT', 0x5554, 4, 32, -1,    false, false, isTextChar,       "trailing", "UText");
  //TODO figure out what to do with these
  static const OBOW = const VR('OBOW', 0,  2,  0, -1,    false, false, null,       null, "OBOW");
  static const USOW = const VR('USOW', 0,  2,  0, -1,    false, false, null,       null, "OSOW");
  static const USSS = const VR('USSS', 0,  2,  0, -1,    false, false, null,       null, "USSS");
  static const USSSOW = const VR('USSSOW', 0,  2,  0, -1,false, false, null,       null, "USSSOW");
  static const NoVR = const VR('NoVR', 0,  0,  0,  0,    false, false, null,       null, null);

  static String vrCodeToString(int vrCode) {
    int b1 = vrCode & 255;
    int b2 = (vrCode >> 8) & 255;
    List list = [b2, b1];
    return ASCII.decode(list);
  }

  static const Map<String, VR> nameToVR =
      const {"AE": VR.AE, "AS": VR.AS, "AT": VR.AT, "BR": VR.BR, "CS": VR.CS, "DA": VR.DA,
             "DS": VR.DS, "DT": VR.DT, "FD": VR.FD, "FL": VR.FL, "IS": VR.IS, "LO": VR.LO,
             "LT": VR.LT, "OB": VR.OB, "OD": VR.OD, "OF": VR.OF, "OW": VR.OW, "PN": VR.PN,
             "SH": VR.SH, "SL": VR.SL, "SQ": VR.SQ, "SS": VR.SS, "ST": VR.ST, "TM": VR.TM,
             "UI": VR.UI, "UL": VR.UL, "UN": VR.UN, "UR": VR.UR, "US": VR.US, "UT": VR.UT};

  static VR lookupVR(String s) => nameToVR[s];

  // [code] is the integer equivalent of the VR string.
  static const Map<int, VR> codeToVRMap = const {
      0x4145: VR.AE, 0x4153: VR.AS, 0x4154: VR.AT, 0x4252: VR.BR, 0x4353: VR.CS,
      0x4441: VR.DA, 0x4453: VR.DS, 0x4454: VR.DT, 0x4644: VR.FD, 0x464c: VR.FL,
      0x4953: VR.IS, 0x4c4f: VR.LO, 0x4c54: VR.LT, 0x4f42: VR.OB, 0x4f44: VR.OD,
      0x4f46: VR.OF, 0x4f57: VR.OW, 0x504e: VR.PN, 0x5348: VR.SH, 0x534c: VR.SL,
      0x5351: VR.SQ, 0x5353: VR.SS, 0x5354: VR.ST, 0x544d: VR.TM, 0x5549: VR.UI,
      0x554c: VR.UL, 0x554e: VR.UN, 0x5552: VR.UR, 0x5553: VR.US, 0x5554: VR.UT };

  /// Returns the VR for the [code]. [code] is the integer equivalent of the two character
  /// VR string.
  static VR codeToVR(int code) => codeToVRMap[code];

  bool get isLarge => vfSize == 4;

  //TODO ** compare nameToVR and stringToVR **
  static VR stringToVR(String s) {
    switch (s) {
      case "AE": return VR.AE;
      case "AS": return VR.AS;
      case "AT": return VR.AT;
      case "BR": return VR.BR;
      case "CS": return VR.CS;
      case "DA": return VR.DA;
      case "DS": return VR.DS;
      case "DT": return VR.DT;
      case "FD": return VR.FD;
      case "FL": return VR.FL;
      case "IS": return VR.IS;
      case "LO": return VR.LO;
      case "LT": return VR.LT;
      case "OB": return VR.OB;
      case "OD": return VR.OD;
      case "OF": return VR.OF;
      case "OW": return VR.OW;
      case "PN": return VR.PN;
      case "SH": return VR.SH;
      case "SL": return VR.SL;
      case "SQ": return VR.SQ;
      case "SS": return VR.SS;
      case "ST": return VR.ST;
      case "TM": return VR.TM;
      case "UI": return VR.UI;
      case "UL": return VR.UL;
      case "UN": return VR.UN;
      case "UR": return VR.UR;
      case "US": return VR.US;
      case "UT": return VR.UT;
      default: return null;
    }
  }

  String toString() {
    return
        "VR.$symbol(code=0x${code.toRadixString(16)}, vfSize=$vfSize, maxLength=$maxLength, pad=$paddingByte, inline=$inlineBinary, name=$name)";
  }

  void checkMaxValueLength(int length) {
    if (length > maxLength)
      throw new RangeError("Value Field length too large for VR=$name");
  }
}
