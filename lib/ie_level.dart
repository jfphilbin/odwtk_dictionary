// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library ie_level;

/**
 * A class that defined the Levels of the DICOM Information Model.
 */
class IELevel {
  final String  name;
  final int     level;

  const IELevel(this.name, this.level);

  //TODO we can probably remove PatientStudies from the hierarchy
  static const PATIENT_STUDIES = const IELevel("PatientStudies", 0);
  static const PATIENT         = const IELevel("Patient", 1);
  static const STUDY           = const IELevel("Study", 2);
  static const SERIES          = const IELevel("Series", 3);
  static const INSTANCE        = const IELevel("Instance", 4);
  static const FRAME           = const IELevel("Frame", 5);
  static const ANY             = const IELevel("Any", 6);

  //TODO verify that this method is correct and needed!
  //TODO How should any be handled?
  static bool contains(IELevel outer, IELevel inner) {
    return (outer.level < inner.level) ? true : false;
  }

  String toString() {
    return "IELevel.$level:$name";
  }
}
