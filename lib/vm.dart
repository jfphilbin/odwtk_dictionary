// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library vm;

/**
 * A class that defined Value Multiplicities and their validators.
 *
 * The Value Multiplicity (VM) of an Attribute defines the minimum, maximum and width
 * of an array of values in an Attribute's Value Field.
 */
class VM {
  //Used to write in gen_table_format
  static final nRows = 16;
  static final nCols = 7;
  // Member fields
  final Symbol  id;
  final String  name;
  final int     min;    // min number of values
  final int     max;    // max number of values, where -1 means any number of values
  final int     width;  // width of Value Field
  final bool    fixed;  // are there a fixed number of values

  // Constructor
  const VM(this.id, this.name, this.min, this.max, this.width, this.fixed);

  // Members
  static const VM_1    = const VM(#VM_1,    "VM_1",    1,  1, 1, true);
  static const VM_1_2  = const VM(#VM_1_2,  "VM_1_2",  1,  2, 1, false);
  static const VM_1_3  = const VM(#VM_1_3,  "VM_1_3",  1,  3, 1, false);
  static const VM_1_8  = const VM(#VM_1_3,  "VM_1_8",  1,  8, 1, false);
  static const VM_1_32 = const VM(#VM_1_32, "VM_1_32", 1, 32, 1, false);
  static const VM_1_99 = const VM(#VM_1_99, "VM_1_99", 1, 99, 1, false);
  static const VM_16   = const VM(#VM_16,   "VM_16",  16, 16, 1, true);
  static const VM_1_n  = const VM(#VM_1_n,  "VM_1_n",  1, -1, 1, false);
  static const VM_2    = const VM(#VM_2,    "VM_2",    2,  2, 1, true);
  static const VM_2_2n = const VM(#VM_2_2n, "VM_2_2n", 2, -1, 2, false);
  static const VM_2_n  = const VM(#VM_2_n,  "VM_2_n",  2, -1, 2, false);
  static const VM_3    = const VM(#VM_3,    "VM_3",    3, 3,  1, true);
  static const VM_3_3n = const VM(#VM_3_3n, "VM_3_3n", 3, -1, 3, false);
  static const VM_3_n  = const VM(#VM_3_n,  "VM_3_n",  3, -1, 3, false);
  static const VM_4    = const VM(#VM_4,    "VM_4",    4,  4, 1, true);
  static const VM_6    = const VM(#VM_6,    "VM_6",    6,  6, 1, true);
  static const VM_6_n  = const VM(#VM_6_n,  "VM_6_n",  6, -1, 1, false);
  static const VM_9    = const VM(#VM_9,    "VM_9",    9,  9, 1, true);
  static const VM_NoVM = const VM(#VM_NoVM, "VM_NoVM", 0,  0, 0, true);

  // Lookup Map
  static const _map = const{"VM_1"    : VM_1,
                            "VM_1_2"  : VM_1_2,
                            "VM_1_32" : VM_1_32,
                            "VM_1_99" : VM_1_99,
                            "VM_16"   : VM_16,
                            "VM_1_n"  : VM_1_n,
                            "VM_2"    : VM_2,
                            "VM_2_2n" : VM_2_2n,
                            "VM_2_n"  : VM_2_n,
                            "VM_3"    : VM_3,
                            "VM_3_3n" : VM_3_3n,
                            "VM_3_n"  : VM_3_n,
                            "VM_4"    : VM_4,
                            "VM_6"    : VM_6,
                            "VM_6_n"  : VM_6_n,
                            "VM_9"    : VM_9 };

  /// lookup VM using name
  static VM lookup(String name) => _map[name];

  /**
   * Validate that the number of values is legal
   */
  //TODO write unit tests to ensure this is correct
  // This is a method specific to VM
  bool validate(List values) {
    int len = values.length;
    print(len);
    int max = (this.max == -1) ? 0x3FFFFFFF : this.max;
    if ((len >= this.min) &&
        (len <= (max * this.width)) &&
        (len%this.width == 0)) {
      return true;
    } else {
      return false;
    }
  }

  //TODO add the other VM definitions
  // Write the class out in gen_table_format
  static void writeToFile(String filename) {
    int nRows = _map.length;
    int nCols = VM.nCols;
  }

  String tableEntry() =>
      'className=VM, nRows=$nRows, nCols=$nCols';
  String fieldNames() =>
      'id, name, min, max, width, fixed';
  String fieldTypes() =>
      'Symbol, String, int, int, int, bool';
  String toLogEntry() =>
    'VM: $id, name=$name, min=$min, max=$max, width=$width, fixed=$fixed';

  String toString() =>
    "Value Multiplicity(VM): $id, Name=$name[$min <= values <= $max by $width]";
}



