// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library fmi_tag_class;

import 'package:dictionary/utilities.dart';
import 'package:dictionary/vr.dart';
import 'package:dictionary/vm.dart';

class FmiTag {
  static const MIN_TAG = FileMetaInformationGroupLength;
  static const MAX_TAG = FileMetaInformationGroupLength;

  final int tag;
  final String keyword;
  final VR vr;
  final VM vm = VM.VM_1;

  const FmiTag(this.tag, this.keyword, this.vr);

  String get name => keywordToName(keyword);

  static const FileMetaInformationGroupLength =
      const FmiTag(0x00020000, "FileMetaInformationGroupLength", VR.UL);

  static const FileMetaInformationVersion =
      const FmiTag(0x00020001, "FileMetaInformationVersion", VR.OB);

  static const MediaStorageSOPClassUID =
      const FmiTag(0x00020002, "MediaStorageSOPClassUID", VR.UI);

  static const MediaStorageSOPInstanceUID =
      const FmiTag(0x00020003, "MediaStorageSOPInstanceUID", VR.UI);

  static const TransferSyntaxUID = const FmiTag(0x00020010, "TransferSyntaxUID", VR.UI);

  static const ImplementationClassUID =
      const FmiTag(0x00020012, "ImplementationClassUID", VR.UI);

  static const ImplementationVersionName =
      const FmiTag(0x00020013, "ImplementationVersionName", VR.SH);

  static const SourceApplicationEntityTitle =
      const FmiTag(0x00020016, "SourceApplicationEntityTitle", VR.AE);

  static const SendingApplicationEntityTitle =
      const FmiTag(0x00020017, "SendingApplicationEntityTitle", VR.AE);

  static const ReceivingApplicationEntityTitle =
      const FmiTag(0x00020018, "ReceivingApplicationEntityTitle", VR.AE);

  static const PrivateInformationCreatorUID =
      const FmiTag(0x00020100, "PrivateInformationCreatorUID", VR.UI);

  static const PrivateInformation = const FmiTag(0x00020102, "PrivateInformation", VR.OB);
}
