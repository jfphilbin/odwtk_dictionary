// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library fmi_constants;

import 'package:dictionary/src/tag/tag_constants.dart';
import 'package:dictionary/utilities.dart';

/*
 * A library for handling DICOM File Meta Information Tags.
 *
 * This library contains top level procedures it should be imported using
 *    "import 'package:core/tag.dart' as Tag;
 */

/*
// **** these definitions moved to tag_constant.dart ****
// File Meta Info Tag constants.
const int $FileMetaInformationGroupLength = 0x00020000;
const int $FileMetaInformationVersion = 0x00020001;
const int $MediaStorageSOPClassUID = 0x00020002;
const int $MediaStorageSOPInstanceUID = 0x00020003;
const int $TransferSyntaxUID = 0x00020010;
const int $ImplementationClassUID = 0x00020012;
const int $ImplementationVersionName = 0x00020013;
const int $SourceApplicationEntityTitle = 0x00020016;
const int $SendingApplicationEntityTitle = 0x00020017;
const int $ReceivingApplicationEntityTitle = 0x00020018;
const int $PrivateInformationCreatorUID = 0x00020100;
const int $PrivateInformation = 0x00020102;

const MIN_FMI_TAG = $FileMetaInformationGroupLength;
const MAX_FMI_TAG = $PrivateInformation;

/// [True] for any File Meta Information [Tag], false otherwise.
/// Note: Does not test flag validity
bool fmiTagInRange(int tag) => (MIN_FMI_TAG <= tag) && (tag <= MAX_FMI_TAG);
*/

const List<int> fmiTags = const [
    $FileMetaInformationGroupLength,
    $FileMetaInformationVersion,
    $MediaStorageSOPClassUID,
    $MediaStorageSOPInstanceUID,
    $TransferSyntaxUID,
    $ImplementationClassUID,
    $ImplementationVersionName,
    $SourceApplicationEntityTitle,
    $SendingApplicationEntityTitle,
    $ReceivingApplicationEntityTitle,
    $PrivateInformationCreatorUID,
    $PrivateInformation];

int fmiTagIndex(int tag) => fmiTags.indexOf(tag);
bool isValidFmiTag(int tag) => fmiTags.contains(tag);

const List<String> fmiKeywords = const [
    "FileMetaInformationGroupLength",
    "FileMetaInformationVersion",
    "MediaStorageSOPClassUID",
    "MediaStorageSOPInstanceUID",
    "TransferSyntaxUID",
    "ImplementationClassUID",
    "ImplementationVersionName",
    "SourceApplicationEntityTitle",
    "SendingApplicationEntityTitle",
    "ReceivingApplicationEntityTitle",
    "PrivateInformationCreatorUID",
    "PrivateInformation"];

int fmiKeywordIndex(String keyword) => fmiKeywords.indexOf(keyword);
bool isValidFmiKeyword(String keyword) => fmiKeywords.contains(keyword);

const Map<String, int> fmiKeywordToTagMap = const {
  "FileMetaInformationGroupLength": $FileMetaInformationGroupLength,
  "FileMetaInformationVersion": $FileMetaInformationVersion,
  "MediaStorageSOPClassUID": $MediaStorageSOPClassUID,
  "MediaStorageSOPInstanceUID": $MediaStorageSOPInstanceUID,
  "TransferSyntaxUID": $TransferSyntaxUID,
  "ImplementationClassUID": $ImplementationClassUID,
  "ImplementationVersionName": $ImplementationVersionName,
  "SourceApplicationEntityTitle": $SourceApplicationEntityTitle,
  "SendingApplicationEntityTitle": $SendingApplicationEntityTitle,
  "ReceivingApplicationEntityTitle": $ReceivingApplicationEntityTitle,
  "PrivateInformationCreatorUID": $PrivateInformationCreatorUID,
  "PrivateInformation": $PrivateInformation
};

int fmiKeywordToTag(String keyword) => fmiKeywordToTagMap[keyword];

const Map<int, String> fmiTagToKeywordMap = const {
    $FileMetaInformationGroupLength: "FileMetaInformationGroupLength",
    $FileMetaInformationVersion: "FileMetaInformationVersion",
    $MediaStorageSOPClassUID: "MediaStorageSOPClassUID",
    $MediaStorageSOPInstanceUID: "MediaStorageSOPInstanceUID",
    $TransferSyntaxUID: "TransferSyntaxUID",
    $ImplementationClassUID: "ImplementationClassUID",
    $ImplementationVersionName: "ImplementationVersionName",
    $SourceApplicationEntityTitle: "SourceApplicationEntityTitle",
    $SendingApplicationEntityTitle: "SendingApplicationEntityTitle",
    $ReceivingApplicationEntityTitle: "ReceivingApplicationEntityTitle",
    $PrivateInformationCreatorUID: "PrivateInformationCreatorUID",
    $PrivateInformation: "PrivateInformation"};

String fmiTagToKeyword(int tag) => fmiTagToKeywordMap[tag];
String fmiTagToName(int tag) => keywordToName(fmiTagToKeywordMap[tag]);





