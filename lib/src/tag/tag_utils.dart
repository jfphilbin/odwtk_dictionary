// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library tag_utils;

import 'package:logging/logging.dart';
import 'package:dictionary/tag.dart';
import 'package:dictionary/vr.dart';
import 'package:utilities/ascii.dart';



// Standard Library definitions
final log = new Logger("dictionary.tag");
String docLink = 'doc/dicom/dart/tag.html';

/**
 * A library for handling DICOM Tags.
 *
 * This library contains top level procedures it should be imported using
 *    "import 'package:core/tag.dart' as Tag;
 */


//**** Tag Group and Element Utilities ****

/// Returns the group number of [tag].
int tagGroup(int tag) => tag >> 16;

/// Returns the [tagGroup] number as a hex [String].
String tagGroupString(int tag) => toHexString(tagGroup(tag), 4);

/// A bit mask, which when bitwise anded with [tag] returns [tagElement].
const int ELEMENT_MASK = 0x0000FFFF;

/// Returns the element number of [tag].
int tagElement(int tag) => tag & ELEMENT_MASK;

/// Returns the element number as a hex [String].
String tagElementString(int tag) => toHexString(tagElement(tag), 4);

/// Takes a [group] and [element] and returns [tag].
//  //TODO check for valid group and element
int tagFromGE(int group, int element) => (group << 16) + element;


//*** Tag String Utilities ***

/// Returns [value] as a hexadecimal string of [length] with prefix [prefix].
String toHexString(int value, int length, {bool prefix: true}) {
  String s = value.toRadixString(16);
  for(int i = s.length; i < length; i++) s = "0" + s;
  return (prefix) ? '0x$s' : s;
}

/** Returns a hex [String] 8 characters long with a "0x" prefix. */
String tagToHex(int tag) {
  tagInRange(tag);
  return intToHex(tag, 8, true);
}

/// Returns [tag] in DICOM format '(gggg,eeee)'.
String tagToDcmFmt(int tag) {
  String g = toHexString(tagGroup(tag), 4, prefix: false);
  String e = toHexString(tagElement(tag), 4, prefix: false);
  return '($g,$e)';
}

/// Returns [tag] as a [String] in hex.
String tagString(int tag) => toHexString(tag, 8);

/** Returns a [List] of DICOM tag codes in '0xggggeeee format */
Iterable<String> tagListToHex(List<int>   list) => list.map(tagToHex);

/** Returns a [List] of DICOM tag codes in '(gggg,eeee)' format */
Iterable<String> tagListToDicom(List<int> list) => list.map(tagToDcmFmt);

/** Returns a [String] in [List<HexTag>] format - '0xggggeeee'. */
IntListToString tagListToHexString = intListToString(tagToHex);

/** Returns a [String] in [List<DcmFmt>] format - '(gggg,eeee)' */
IntListToString tagListToDcmFmt    = intListToString(tagToDcmFmt);


//**** Sequence Utilities ****
bool isSequenceTag(int tag)    => tagToTagClassMap[tag].vr == VR.SQ;
bool isSequenceEndTag(int tag) => tag == Tag.SequenceDelimitationItem;

// Items
/// Returns [true] if [tag] is  ItemTag
bool isItemTag(int tag)    => (tag == Tag.Item);  // 0xFFFE,E000
bool isItemEndTag(int tag) => tag == Tag.ItemDelimitationItem;


// Tag Validators
// Internal implementation of Tag Validators
typedef bool TagTest(tag);

TagTest testTagInRange(int min, int max) =>
    (int tag) {
      ((min <= tag) && (tag <= max)) ? true : _tagOutOfRange(tag, min, max);
    };

bool _tagOutOfRange(int tag, int min, int max) {
  String msg = 'invalid tag: $tag not in $min <= x <= $max';
  log.severe(msg);
  throw new ArgumentError(msg);
}

/// The minimum legal tag value.
const int $MinTag = $FileMetaInformationGroupLength; //(00020000)

/// The miximum legal tag value.
const int $MaxTag = $EndOfSequence; // (FFFE,E0DD)
/// True for any Tag in the DICOM range, false otherwise.
//tagInRange(int tag) => (tag >= $MinTag) && (tag <= $MinTag);
TagTest tagInRange = testTagInRange($MinTag, $MaxTag);

/// The minimum File Meta Info tag value.
const $MinFmiTag = $FileMetaInformationGroupLength;

/// The miximum File Meta Info tag value.
const $MaxFmiTag = $PrivateInformation;

/// [True] for any File Meta Information [Tag], false otherwise.
/// Note: Does not test flag validity
//bool fmiTagInRange(int tag) => ($MinFmiTag <= tag) && (tag <= $MaxFmiTag);
TagTest fmiTagInRange = testTagInRange($MinFmiTag, $MaxFmiTag);

/// The minimum DICOM Dir tag value.
const int $MinDcmDirTag = $FileSetID;

/// The miximum DICOM Dir tag value.
const int $MaxDcmDirTag = $NumberOfReferences;

/// Returns [true] if [tag] is in the range of DICOM Directory Tags
//bool dcmDirTagInRange(int tag) => tag >= $MinDcmDirTag && tag <= $MaxDcmDirTag;
TagTest dcmDirTagInRange = testTagInRange($MinDcmDirTag, $MaxDcmDirTag);

/// The minimum Composite Dataset tag value.
const int $MinDatasetTag = $LengthToEnd;

/// The miximum Composite Dataset tag value.
const int $MaxDatasetTag = $EndOfSequence;

/// [True] for any [Tag] that can be contained in a general [Dataset], false otherwise.
//dsTagInRange(int tag) => (tag >= MIN_DATASET_TAG) && (tag <= MAX_DATASET_TAG);
TagTest dsTagInRange = testTagInRange($MinDatasetTag, $MaxDatasetTag);



