Generated Code
==============

The files in this directory [core_toolkit/data_types/lib/gen] are generated from tables
in the DICOM standard.  The code that generates these files is in 
[core_toolkit/data_types/generators].  Details can be found in the files there.