ToDo file for Tag Library

1. Figure out how to handle tags with "x"s in them.  They need special handling
2. Create the following compile time constant objects
    a. Map<int, String> tagToNameTable
    b. Map<int, int> tagToIndex
    c. List<int> tagIndexToTagData - this is a list of 32 bit integers that specify
      12 bits (0 - 12) tag index
      6  bits (12 - 18) vr index
      5  bits (18 - 23) vm index 
      3  bits (23 - 26) type 
      3  bits (26 - 29) class 
      1  bit  29        isRetired
    d. Map<int, int>  tagToVRIndex
    e. Map<int, int>  tagToVMIndex
 3. Recreate the tagClass with extra values for class (study, series...), and index.