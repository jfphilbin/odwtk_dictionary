// DCMiD Project"
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dcm_dir_constants;

import 'package:dictionary/src/tag/tag_constants.dart';

/**
 * A library of compile time constants for handling DICOM Directory data
 */

//**** these definitions have been moved to src/tag/tag_constants.dart  ****
/*
/// DICOM Directory Tag definitions.  All DICOM Dir tags have a group number of 0x0004.
const int $FileSetID = 0x00041130;
const int $FileSetDescriptorFileID = 0x00041141;
const int $SpecificCharacterSetOfFileSetDescriptorFile = 0x00041142;
const int $OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity = 0x00041200;
const int $OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity = 0x00041202;
const int $FileSetConsistencyFlag = 0x00041212;
const int $DirectoryRecordSequence = 0x00041220;
const int $OffsetOfTheNextDirectoryRecord = 0x00041400;
const int $RecordInUseFlag = 0x00041410;
const int $OffsetOfReferencedLowerLevelDirectoryEntity = 0x00041420;
const int $DirectoryRecordType = 0x00041430;
const int $PrivateRecordUID = 0x00041432;
const int $ReferencedFileID = 0x00041500;
const int $MRDRDirectoryRecordOffset = 0x00041504;
const int $ReferencedSOPClassUIDInFile = 0x00041510;
const int $ReferencedSOPInstanceUIDInFile = 0x00041511;
const int $ReferencedTransferSyntaxUIDInFile = 0x00041512;
const int $ReferencedRelatedGeneralSOPClassUIDInFile = 0x0004151A;
const int $NumberOfReferences = 0x00041600;

/// The minimum DICOM Dir tag value.
const int $MinDcmDirTag = $FileSetID;

/// The miximum DICOM Dir tag value.
const int $MaxDcmDirTag = $NumberOfReferences;

/// Returns [true] if [tag] is in the range of DICOM Directory Tags
bool dcmDirTagInRange(int tag) => tag >= $MinDcmDirTag && tag <= $MaxDcmDirTag;
*/


/// A [Map<int, String>] of [tag]:[keyword] pairs.
const Map<int, String> dcmDirTagToKeywordMap = const {
  0x00041130: "FileSetID",
  0x00041141: "FileSetDescriptorFileID",
  0x00041142: "SpecificCharacterSetOfFileSetDescriptorFile",
  0x00041200: "OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity",
  0x00041202: "OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity",
  0x00041212: "FileSetConsistencyFlag",
  0x00041220: "DirectoryRecordSequence",
  0x00041400: "OffsetOfTheNextDirectoryRecord",
  0x00041410: "RecordInUseFlag",
  0x00041420: "OffsetOfReferencedLowerLevelDirectoryEntity",
  0x00041430: "DirectoryRecordType",
  0x00041432: "PrivateRecordUID",
  0x00041500: "ReferencedFileID",
  0x00041504: "MRDRDirectoryRecordOffset",
  0x00041510: "ReferencedSOPClassUIDInFile",
  0x00041511: "ReferencedSOPInstanceUIDInFile",
  0x00041512: "ReferencedTransferSyntaxUIDInFile",
  0x0004151A: "ReferencedRelatedGeneralSOPClassUIDInFile",
  0x00041600: "NumberOfReferences",
};

const Map<String, int> dcmDirKeywordToTagMap = const {
  "FileSetID": 0x00041130,
  "FileSetDescriptorFileID": 0x00041141,
  "SpecificCharacterSetOfFileSetDescriptorFile": 0x00041142,
  "OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity": 0x00041200,
  "OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity": 0x00041202,
  "FileSetConsistencyFlag": 0x00041212,
  "DirectoryRecordSequence": 0x00041220,
  "OffsetOfTheNextDirectoryRecord": 0x00041400,
  "RecordInUseFlag": 0x00041410,
  "OffsetOfReferencedLowerLevelDirectoryEntity": 0x00041420,
  "DirectoryRecordType": 0x00041430,
  "PrivateRecordUID": 0x00041432,
  "ReferencedFileID": 0x00041500,
  "MRDRDirectoryRecordOffset": 0x00041504,
  "ReferencedSOPClassUIDInFile": 0x00041510,
  "ReferencedSOPInstanceUIDInFile": 0x00041511,
  "ReferencedTransferSyntaxUIDInFile": 0x00041512,
  "ReferencedRelatedGeneralSOPClassUIDInFile": 0x0004151A,
  "NumberOfReferences": 0x00041600,
};

const Map dcmDirKeywordToTagStringMap = const {
  "FileSetID": "0x00041130",
  "FileSetDescriptorFileID": "0x00041141",
  "SpecificCharacterSetOfFileSetDescriptorFile": "0x00041142",
  "OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity": "0x00041200",
  "OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity": "0x00041202",
  "FileSetConsistencyFlag": "0x00041212",
  "DirectoryRecordSequence": "0x00041220",
  "OffsetOfTheNextDirectoryRecord": "0x00041400",
  "RecordInUseFlag": "0x00041410",
  "OffsetOfReferencedLowerLevelDirectoryEntity": "0x00041420",
  "DirectoryRecordType": "0x00041430",
  "PrivateRecordUID": "0x00041432",
  "ReferencedFileID": "0x00041500",
  "MRDRDirectoryRecordOffset": "0x00041504",
  "ReferencedSOPClassUIDInFile": "0x00041510",
  "ReferencedSOPInstanceUIDInFile": "0x00041511",
  "ReferencedTransferSyntaxUIDInFile": "0x00041512",
  "ReferencedRelatedGeneralSOPClassUIDInFile": "0x0004151A",
  "NumberOfReferences": "0x00041600",
};

const List<int> dcmDirTagList = const [
  $FileSetID, // (0004,1130)
  $FileSetDescriptorFileID, // (0004,1141)
  $SpecificCharacterSetOfFileSetDescriptorFile, // (0004,1142)
  $OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity, // (0004,1200)
  $OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity, // (0004,1202)
  $FileSetConsistencyFlag, // (0004,1212)
  $DirectoryRecordSequence, // (0004,1220)
  $OffsetOfTheNextDirectoryRecord, // (0004,1400)
  $RecordInUseFlag, // (0004,1410)
  $OffsetOfReferencedLowerLevelDirectoryEntity, // (0004,1420)
  $DirectoryRecordType, // (0004,1430)
  $PrivateRecordUID, // (0004,1432)
  $ReferencedFileID, // (0004,1500)
  $MRDRDirectoryRecordOffset, // (0004,1504)
  $ReferencedSOPClassUIDInFile, // (0004,1510)
  $ReferencedSOPInstanceUIDInFile, // (0004,1511)
  $ReferencedTransferSyntaxUIDInFile, // (0004,1512)
  $ReferencedRelatedGeneralSOPClassUIDInFile, // (0004,151A)
  $NumberOfReferences, // (0004,1600)
];

/// Returns true if [dcmDirTagList] contains [tag].
bool isValidDcmDirTag(int tag) => dcmDirTagList.contains(tag);

/// A [tag] ordered list of DICOM Directory Keywords.
const List<int> dcmDirKeywordList = const [
    "FileSetID",
    "FileSetDescriptorFileID",
    "SpecificCharacterSetOfFileSetDescriptorFile",
    "OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity",
    "OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity",
    "FileSetConsistencyFlag",
    "DirectoryRecordSequence",
    "OffsetOfTheNextDirectoryRecord",
    "RecordInUseFlag",
    "OffsetOfReferencedLowerLevelDirectoryEntity",
    "DirectoryRecordType",
    "PrivateRecordUID",
    "ReferencedFileID",
    "MRDRDirectoryRecordOffset",
    "ReferencedSOPClassUIDInFile",
    "ReferencedSOPInstanceUIDInFile",
    "ReferencedTransferSyntaxUIDInFile",
    "ReferencedRelatedGeneralSOPClassUIDInFile",
    "NumberOfReferences"];

/// Returns true if [dcmDirKeywordList] contains [keyword].
bool isValidDcmDirKeyword(String keyword ) => dcmDirTagList.contains(keyword);
