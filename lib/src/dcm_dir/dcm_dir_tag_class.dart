// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dcm_dir_tag_class ;

import 'package:dictionary/vr.dart';
import 'package:dictionary/vm.dart';

/**
 * A compile time constant class implementing the DICOM Directory Data Element definitions.
 */
class DcmDirTag {
  //TODO define the class
  final int    tag;
  final String keyword;
  final VR     vr;
  final VM     vm;
  final bool   isRetired;

  const DcmDirTag(this.tag, this.keyword, this.vr, this.vm, this.isRetired);

  static const FileSetID =
      const DcmDirTag(0x00041130, "FileSetID", VR.CS, VM.VM_1, false);
  static const FileSetDescriptorFileID =
      const DcmDirTag(0x00041141, "FileSetDescriptorFileID", VR.CS, VM.VM_1_8, false);
  static const SpecificCharacterSetOfFileSetDescriptorFile =
      const DcmDirTag(0x00041142, "SpecificCharacterSetOfFileSetDescriptorFile", VR.CS, VM.VM_1, false);
  static const OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity =
      const DcmDirTag(0x00041200, "OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity", VR.UL, VM.VM_1, false);
  static const OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity =
      const DcmDirTag(0x00041202, "OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity", VR.UL, VM.VM_1, false);
  static const FileSetConsistencyFlag=
      const DcmDirTag(0x00041212, "FileSetConsistencyFlag", VR.US, VM.VM_1, false);
  static const DirectoryRecordSequence =
      const DcmDirTag(0x00041220, "DirectoryRecordSequence", VR.SQ, VM.VM_1, false);
  static const OffsetOfTheNextDirectoryRecord =
      const DcmDirTag(0x00041400, "OffsetOfTheNextDirectoryRecord", VR.UL, VM.VM_1, false);
  static const RecordInUseFlag =
      const DcmDirTag(0x00041410, "RecordInUseFlag", VR.US, VM.VM_1, false);
  static const OffsetOfReferencedLowerLevelDirectoryEntity=
      const DcmDirTag(0x00041420, "OffsetOfReferencedLowerLevelDirectoryEntity", VR.UL, VM.VM_1, false);
  static const DirectoryRecordType =
      const DcmDirTag(0x00041430, "DirectoryRecordType", VR.CS, VM.VM_1, false);
  static const PrivateRecordUID =
      const DcmDirTag(0x00041432, "PrivateRecordUID", VR.UI, VM.VM_1, false);
  static const ReferencedFileID =
      const DcmDirTag(0x00041500, "ReferencedFileID", VR.CS, VM.VM_1_8, false);
  static const MRDRDirectoryRecordOffset =
      const DcmDirTag(0x00041504, "MRDRDirectoryRecordOffset", VR.UL, VM.VM_1, true);
  static const ReferencedSOPClassUIDInFile=
      const DcmDirTag(0x00041510, "ReferencedSOPClassUIDInFile", VR.UI, VM.VM_1, false);
  static const ReferencedSOPInstanceUIDInFile =
      const DcmDirTag(0x00041511, "ReferencedSOPInstanceUIDInFile", VR.UI, VM.VM_1, false);
  static const ReferencedTransferSyntaxUIDInFile=
      const DcmDirTag(0x00041512, "ReferencedTransferSyntaxUIDInFile", VR.UI, VM.VM_1, false);
  static const ReferencedRelatedGeneralSOPClassUIDInFile =
      const DcmDirTag(0x0004151A, "ReferencedRelatedGeneralSOPClassUIDInFile", VR.UI, VM.VM_1_n, false);
  static const NumberOfReferences =
      const DcmDirTag(0x00041600, "NumberOfReferences", VR.UL, VM.VM_1, true);

  static const List<int> dcmDirTagList = const [
    FileSetID, // (0004,1130)
    FileSetDescriptorFileID, // (0004,1141)
    SpecificCharacterSetOfFileSetDescriptorFile, // (0004,1142)
    OffsetOfTheFirstDirectoryRecordOfTheRootDirectoryEntity, // (0004,1200)
    OffsetOfTheLastDirectoryRecordOfTheRootDirectoryEntity, // (0004,1202)
    FileSetConsistencyFlag, // (0004,1212)
    DirectoryRecordSequence, // (0004,1220)
    OffsetOfTheNextDirectoryRecord, // (0004,1400)
    RecordInUseFlag, // (0004,1410)
    OffsetOfReferencedLowerLevelDirectoryEntity, // (0004,1420)
    DirectoryRecordType, // (0004,1430)
    PrivateRecordUID, // (0004,1432)
    ReferencedFileID, // (0004,1500)
    MRDRDirectoryRecordOffset, // (0004,1504)
    ReferencedSOPClassUIDInFile, // (0004,1510)
    ReferencedSOPInstanceUIDInFile, // (0004,1511)
    ReferencedTransferSyntaxUIDInFile, // (0004,1512)
    ReferencedRelatedGeneralSOPClassUIDInFile, // (0004,151A)
    NumberOfReferences, // (0004,1600)
  ];
}