// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library uid_to_uid_def_map;

import 'package:dictionary/uid.dart';

const Map<String, String> $UIDToUIDDefMap = const  {
"1.2.840.10008.1.1": UID.VerificationSOPClass,
"1.2.840.10008.1.2": UID.ImplicitVRLittleEndianDefaultTransferSyntaxforDICOM,
"1.2.840.10008.1.2.1": UID.ExplicitVRLittleEndian,
"1.2.840.10008.1.2.1.99": UID.DeflatedExplicitVRLittleEndian,
"1.2.840.10008.1.2.2": UID.ExplicitVRBigEndian_Retired,
"1.2.840.10008.1.2.4.50": UID.JPEGBaseline_1DefaultTransferSyntaxforLossyJPEG8BitImageCompression,
"1.2.840.10008.1.2.4.51": UID.JPEGExtended_2_4DefaultTransferSyntaxforLossyJPEG12BitImageCompression_4,
"1.2.840.10008.1.2.4.52": UID.JPEGExtended_3_5_Retired,
"1.2.840.10008.1.2.4.53": UID.JPEGSpectralSelectionNon_Hierarchical_6_8_Retired,
"1.2.840.10008.1.2.4.54": UID.JPEGSpectralSelectionNon_Hierarchical_7_9_Retired,
"1.2.840.10008.1.2.4.55": UID.JPEGFullProgressionNon_Hierarchical_10_12_Retired,
"1.2.840.10008.1.2.4.56": UID.JPEGFullProgressionNon_Hierarchical_11_13_Retired,
"1.2.840.10008.1.2.4.57": UID.JPEGLosslessNon_Hierarchical_14,
"1.2.840.10008.1.2.4.58": UID.JPEGLosslessNon_Hierarchical_15_Retired,
"1.2.840.10008.1.2.4.59": UID.JPEGExtendedHierarchical_16_18_Retired,
"1.2.840.10008.1.2.4.60": UID.JPEGExtendedHierarchical_17_19_Retired,
"1.2.840.10008.1.2.4.61": UID.JPEGSpectralSelectionHierarchical_20_22_Retired,
"1.2.840.10008.1.2.4.62": UID.JPEGSpectralSelectionHierarchical_21_23_Retired,
"1.2.840.10008.1.2.4.63": UID.JPEGFullProgressionHierarchical_24_26_Retired,
"1.2.840.10008.1.2.4.64": UID.JPEGFullProgressionHierarchical_25_27_Retired,
"1.2.840.10008.1.2.4.65": UID.JPEGLosslessHierarchical_28_Retired,
"1.2.840.10008.1.2.4.66": UID.JPEGLosslessHierarchical_29_Retired,
"1.2.840.10008.1.2.4.70": UID.JPEGLosslessNon_HierarchicalFirst_OrderPrediction_14_1DefaultTransferSyntaxforLosslessJPEGImageCompression,
"1.2.840.10008.1.2.4.80": UID.JPEG_LSLosslessImageCompression,
"1.2.840.10008.1.2.4.81": UID.JPEG_LSLossyImageCompression,
"1.2.840.10008.1.2.4.90": UID.JPEG2000ImageCompressionLosslessOnly,
"1.2.840.10008.1.2.4.91": UID.JPEG2000ImageCompression,
"1.2.840.10008.1.2.4.92": UID.JPEG2000Part2Multi_componentImageCompressionLosslessOnly,
"1.2.840.10008.1.2.4.93": UID.JPEG2000Part2Multi_componentImageCompression,
"1.2.840.10008.1.2.4.94": UID.JPIPReferenced,
"1.2.840.10008.1.2.4.95": UID.JPIPReferencedDeflate,
"1.2.840.10008.1.2.4.100": UID.MPEG2MainProfile_MainLevel,
"1.2.840.10008.1.2.4.101": UID.MPEG2MainProfile_HighLevel,
"1.2.840.10008.1.2.4.102": UID.MPEG_4AVC_H264HighProfile_Level41,
"1.2.840.10008.1.2.4.103": UID.MPEG_4AVC_H264BD_compatibleHighProfile_Level41,
"1.2.840.10008.1.2.5": UID.RLELossless,
"1.2.840.10008.1.2.6.1": UID.RFC2557MIMEencapsulation,
"1.2.840.10008.1.2.6.2": UID.XMLEncoding,
"1.2.840.10008.1.3.10": UID.MediaStorageDirectoryStorage,
"1.2.840.10008.1.4.1.1": UID.TalairachBrainAtlasFrameofReference,
"1.2.840.10008.1.4.1.2": UID.SPM2T1FrameofReference,
"1.2.840.10008.1.4.1.3": UID.SPM2T2FrameofReference,
"1.2.840.10008.1.4.1.4": UID.SPM2PDFrameofReference,
"1.2.840.10008.1.4.1.5": UID.SPM2EPIFrameofReference,
"1.2.840.10008.1.4.1.6": UID.SPM2FILT1FrameofReference,
"1.2.840.10008.1.4.1.7": UID.SPM2PETFrameofReference,
"1.2.840.10008.1.4.1.8": UID.SPM2TRANSMFrameofReference,
"1.2.840.10008.1.4.1.9": UID.SPM2SPECTFrameofReference,
"1.2.840.10008.1.4.1.10": UID.SPM2GRAYFrameofReference,
"1.2.840.10008.1.4.1.11": UID.SPM2WHITEFrameofReference,
"1.2.840.10008.1.4.1.12": UID.SPM2CSFFrameofReference,
"1.2.840.10008.1.4.1.13": UID.SPM2BRAINMASKFrameofReference,
"1.2.840.10008.1.4.1.14": UID.SPM2AVG305T1FrameofReference,
"1.2.840.10008.1.4.1.15": UID.SPM2AVG152T1FrameofReference,
"1.2.840.10008.1.4.1.16": UID.SPM2AVG152T2FrameofReference,
"1.2.840.10008.1.4.1.17": UID.SPM2AVG152PDFrameofReference,
"1.2.840.10008.1.4.1.18": UID.SPM2SINGLESUBJT1FrameofReference,
"1.2.840.10008.1.4.2.1": UID.ICBM452T1FrameofReference,
"1.2.840.10008.1.4.2.2": UID.ICBMSingleSubjectMRIFrameofReference,
"1.2.840.10008.1.5.1": UID.HotIronColorPaletteSOPInstance,
"1.2.840.10008.1.5.2": UID.PETColorPaletteSOPInstance,
"1.2.840.10008.1.5.3": UID.HotMetalBlueColorPaletteSOPInstance,
"1.2.840.10008.1.5.4": UID.PET20StepColorPaletteSOPInstance,
"1.2.840.10008.1.9": UID.BasicStudyContentNotificationSOPClass_Retired,
"1.2.840.10008.1.20.1": UID.StorageCommitmentPushModelSOPClass,
"1.2.840.10008.1.20.1.1": UID.StorageCommitmentPushModelSOPInstance,
"1.2.840.10008.1.20.2": UID.StorageCommitmentPullModelSOPClass_Retired,
"1.2.840.10008.1.20.2.1": UID.StorageCommitmentPullModelSOPInstance_Retired,
"1.2.840.10008.1.40": UID.ProceduralEventLoggingSOPClass,
"1.2.840.10008.1.40.1": UID.ProceduralEventLoggingSOPInstance,
"1.2.840.10008.1.42": UID.SubstanceAdministrationLoggingSOPClass,
"1.2.840.10008.1.42.1": UID.SubstanceAdministrationLoggingSOPInstance,
"1.2.840.10008.2.6.1": UID.DICOMUIDRegistry,
"1.2.840.10008.2.16.4": UID.DICOMControlledTerminology,
"1.2.840.10008.3.1.1.1": UID.DICOMApplicationContextName,
"1.2.840.10008.3.1.2.1.1": UID.DetachedPatientManagementSOPClass_Retired,
"1.2.840.10008.3.1.2.1.4": UID.DetachedPatientManagementMetaSOPClass_Retired,
"1.2.840.10008.3.1.2.2.1": UID.DetachedVisitManagementSOPClass_Retired,
"1.2.840.10008.3.1.2.3.1": UID.DetachedStudyManagementSOPClass_Retired,
"1.2.840.10008.3.1.2.3.2": UID.StudyComponentManagementSOPClass_Retired,
"1.­2.840.10008.3.1.2.3.3": UID.ModalityPerformedProcedureStepSOPClass,
"1.­2.840.10008.3.1.2.3.4": UID.ModalityPerformedProcedureStepRetrieveSOPClass,
"1.­2.840.10008.3.1.2.3.5": UID.ModalityPerformedProcedureStepNotificationSOPClass,
"1.2.840.10008.3.1.2.5.1": UID.DetachedResultsManagementSOPClass_Retired,
"1.2.840.10008.3.1.2.5.4": UID.DetachedResultsManagementMetaSOPClass_Retired,
"1.2.840.10008.3.1.2.5.5": UID.DetachedStudyManagementMetaSOPClass_Retired,
"1.2.840.10008.3.1.2.6.1": UID.DetachedInterpretationManagementSOPClass_Retired,
"1.2.840.10008.4.2": UID.StorageServiceClass,
"1.2.840.10008.5.1.1.1": UID.BasicFilmSessionSOPClass,
"1.2.840.10008.5.1.1.2": UID.BasicFilmBoxSOPClass,
"1.2.840.10008.5.1.1.4": UID.BasicGrayscaleImageBoxSOPClass,
"1.2.840.10008.5.1.1.4.1": UID.BasicColorImageBoxSOPClass,
"1.2.840.10008.5.1.1.4.2": UID.ReferencedImageBoxSOPClass_Retired,
"1.2.840.10008.5.1.1.9": UID.BasicGrayscalePrintManagementMetaSOPClass,
"1.2.840.10008.5.1.1.9.1": UID.ReferencedGrayscalePrintManagementMetaSOPClass_Retired,
"1.2.840.10008.5.1.1.14": UID.PrintJobSOPClass,
"1.2.840.10008.5.1.1.15": UID.BasicAnnotationBoxSOPClass,
"1.2.840.10008.5.1.1.16": UID.PrinterSOPClass,
"1.2.840.10008.5.1.1.16.376": UID.PrinterConfigurationRetrievalSOPClass,
"1.2.840.10008.5.1.1.17": UID.PrinterSOPInstance,
"1.2.840.10008.5.1.1.17.376": UID.PrinterConfigurationRetrievalSOPInstance,
"1.2.840.10008.5.1.1.18": UID.BasicColorPrintManagementMetaSOPClass,
"1.2.840.10008.5.1.1.18.1": UID.ReferencedColorPrintManagementMetaSOPClass_Retired,
"1.2.840.10008.5.1.1.22": UID.VOILUTBoxSOPClass,
"1.2.840.10008.5.1.1.23": UID.PresentationLUTSOPClass,
"1.2.840.10008.5.1.1.24": UID.ImageOverlayBoxSOPClass_Retired,
"1.2.840.10008.5.1.1.24.1": UID.BasicPrintImageOverlayBoxSOPClass_Retired,
"1.2.840.10008.5.1.1.25": UID.PrintQueueSOPInstance_Retired,
"1.2.840.10008.5.1.1.26": UID.PrintQueueManagementSOPClass_Retired,
"1.2.840.10008.5.1.1.27": UID.StoredPrintStorageSOPClass_Retired,
"1.2.840.10008.5.1.1.29": UID.HardcopyGrayscaleImageStorageSOPClass_Retired,
"1.2.840.10008.5.1.1.30": UID.HardcopyColorImageStorageSOPClass_Retired,
"1.2.840.10008.5.1.1.31": UID.PullPrintRequestSOPClass_Retired,
"1.2.840.10008.5.1.1.32": UID.PullStoredPrintManagementMetaSOPClass_Retired,
"1.2.840.10008.5.1.1.33": UID.MediaCreationManagementSOPClassUID,
"1.2.840.10008.5.1.4.1.1.1": UID.ComputedRadiographyImageStorage,
"1.2.840.10008.5.1.4.1.1.1.1": UID.DigitalX_RayImageStorage_ForPresentation,
"1.2.840.10008.5.1.4.1.1.1.1.1": UID.DigitalX_RayImageStorage_ForProcessing,
"1.2.840.10008.5.1.4.1.1.1.2": UID.DigitalMammographyX_RayImageStorage_ForPresentation,
"1.2.840.10008.5.1.4.1.1.1.2.1": UID.DigitalMammographyX_RayImageStorage_ForProcessing,
"1.2.840.10008.5.1.4.1.1.1.3": UID.DigitalIntra_OralX_RayImageStorage_ForPresentation,
"1.2.840.10008.5.1.4.1.1.1.3.1": UID.DigitalIntra_OralX_RayImageStorage_ForProcessing,
"1.2.840.10008.5.1.4.1.1.2": UID.CTImageStorage,
"1.2.840.10008.5.1.4.1.1.2.1": UID.EnhancedCTImageStorage,
"1.2.840.10008.5.1.4.1.1.2.2": UID.LegacyConvertedEnhancedCTImageStorage,
"1.2.840.10008.5.1.4.1.1.3": UID.UltrasoundMulti_frameImageStorage_Retired,
"1.2.840.10008.5.1.4.1.1.3.1": UID.UltrasoundMulti_frameImageStorage,
"1.2.840.10008.5.1.4.1.1.4": UID.MRImageStorage,
"1.2.840.10008.5.1.4.1.1.4.1": UID.EnhancedMRImageStorage,
"1.2.840.10008.5.1.4.1.1.4.2": UID.MRSpectroscopyStorage,
"1.2.840.10008.5.1.4.1.1.4.3": UID.EnhancedMRColorImageStorage,
"1.2.840.10008.5.1.4.1.1.4.4": UID.LegacyConvertedEnhancedMRImageStorage,
"1.2.840.10008.5.1.4.1.1.5": UID.NuclearMedicineImageStorage_Retired,
"1.2.840.10008.5.1.4.1.1.6": UID.UltrasoundImageStorage_Retired,
"1.2.840.10008.5.1.4.1.1.6.1": UID.UltrasoundImageStorage,
"1.2.840.10008.5.1.4.1.1.6.2": UID.EnhancedUSVolumeStorage,
"1.2.840.10008.5.1.4.1.1.7": UID.SecondaryCaptureImageStorage,
"1.2.840.10008.5.1.4.1.1.7.1": UID.Multi_frameSingleBitSecondaryCaptureImageStorage,
"1.2.840.10008.5.1.4.1.1.7.2": UID.Multi_frameGrayscaleByteSecondaryCaptureImageStorage,
"1.2.840.10008.5.1.4.1.1.7.3": UID.Multi_frameGrayscaleWordSecondaryCaptureImageStorage,
"1.2.840.10008.5.1.4.1.1.7.4": UID.Multi_frameTrueColorSecondaryCaptureImageStorage,
"1.2.840.10008.5.1.4.1.1.8": UID.StandaloneOverlayStorage_Retired,
"1.2.840.10008.5.1.4.1.1.9": UID.StandaloneCurveStorage_Retired,
"1.2.840.10008.5.1.4.1.1.9.1": UID.WaveformStorage_Trial_Retired,
"1.2.840.10008.5.1.4.1.1.9.1.1": UID.twelve_lead_12ECGWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.1.2": UID.GeneralECGWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.1.3": UID.AmbulatoryECGWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.2.1": UID.HemodynamicWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.3.1": UID.CardiacElectrophysiologyWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.4.1": UID.BasicVoiceAudioWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.4.2": UID.GeneralAudioWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.5.1": UID.ArterialPulseWaveformStorage,
"1.2.840.10008.5.1.4.1.1.9.6.1": UID.RespiratoryWaveformStorage,
"1.2.840.10008.5.1.4.1.1.10": UID.StandaloneModalityLUTStorage_Retired,
"1.2.840.10008.5.1.4.1.1.11": UID.StandaloneVOILUTStorage_Retired,
"1.2.840.10008.5.1.4.1.1.11.1": UID.GrayscaleSoftcopyPresentationStateStorageSOPClass,
"1.2.840.10008.5.1.4.1.1.11.2": UID.ColorSoftcopyPresentationStateStorageSOPClass,
"1.2.840.10008.5.1.4.1.1.11.3": UID.Pseudo_ColorSoftcopyPresentationStateStorageSOPClass,
"1.2.840.10008.5.1.4.1.1.11.4": UID.BlendingSoftcopyPresentationStateStorageSOPClass,
"1.2.840.10008.5.1.4.1.1.11.5": UID.XA_XRFGrayscaleSoftcopyPresentationStateStorage,
"1.2.840.10008.5.1.4.1.1.12.1": UID.X_RayAngiographicImageStorage,
"1.2.840.10008.5.1.4.1.1.12.1.1": UID.EnhancedXAImageStorage,
"1.2.840.10008.5.1.4.1.1.12.2": UID.X_RayRadiofluoroscopicImageStorage,
"1.2.840.10008.5.1.4.1.1.12.2.1": UID.EnhancedXRFImageStorage,
"1.2.840.10008.5.1.4.1.1.12.3": UID.X_RayAngiographicBi_PlaneImageStorage_Retired,
"1.2.840.10008.5.1.4.1.1.13.1.1": UID.X_Ray3DAngiographicImageStorage,
"1.2.840.10008.5.1.4.1.1.13.1.2": UID.X_Ray3DCraniofacialImageStorage,
"1.2.840.10008.5.1.4.1.1.13.1.3": UID.BreastTomosynthesisImageStorage,
"1.2.840.10008.5.1.4.1.1.14.1": UID.IntravascularOpticalCoherenceTomographyImageStorage_ForPresentation,
"1.2.840.10008.5.1.4.1.1.14.2": UID.IntravascularOpticalCoherenceTomographyImageStorage_ForProcessing,
"1.2.840.10008.5.1.4.1.1.20": UID.NuclearMedicineImageStorage,
"1.2.840.10008.5.1.4.1.1.66": UID.RawDataStorage,
"1.2.840.10008.5.1.4.1.1.66.1": UID.SpatialRegistrationStorage,
"1.2.840.10008.5.1.4.1.1.66.2": UID.SpatialFiducialsStorage,
"1.2.840.10008.5.1.4.1.1.66.3": UID.DeformableSpatialRegistrationStorage,
"1.2.840.10008.5.1.4.1.1.66.4": UID.SegmentationStorage,
"1.2.840.10008.5.1.4.1.1.66.5": UID.SurfaceSegmentationStorage,
"1.2.840.10008.5.1.4.1.1.67": UID.RealWorldValueMappingStorage,
"1.2.840.10008.5.1.4.1.1.68.1": UID.SurfaceScanMeshStorage,
"1.2.840.10008.5.1.4.1.1.68.2": UID.SurfaceScanPointCloudStorage,
"1.2.840.10008.5.1.4.1.1.77.1": UID.VLImageStorage_Trial_Retired,
"1.2.840.10008.5.1.4.1.1.77.2": UID.VLMulti_frameImageStorage_Trial_Retired,
"1.2.840.10008.5.1.4.1.1.77.1.1": UID.VLEndoscopicImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.1.1": UID.VideoEndoscopicImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.2": UID.VLMicroscopicImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.2.1": UID.VideoMicroscopicImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.3": UID.VLSlide_CoordinatesMicroscopicImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.4": UID.VLPhotographicImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.4.1": UID.VideoPhotographicImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.5.1": UID.OphthalmicPhotography8BitImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.5.2": UID.OphthalmicPhotography16BitImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.5.3": UID.StereometricRelationshipStorage,
"1.2.840.10008.5.1.4.1.1.77.1.5.4": UID.OphthalmicTomographyImageStorage,
"1.2.840.10008.5.1.4.1.1.77.1.6": UID.VLWholeSlideMicroscopyImageStorage,
"1.2.840.10008.5.1.4.1.1.78.1": UID.LensometryMeasurementsStorage,
"1.2.840.10008.5.1.4.1.1.78.2": UID.AutorefractionMeasurementsStorage,
"1.2.840.10008.5.1.4.1.1.78.3": UID.KeratometryMeasurementsStorage,
"1.2.840.10008.5.1.4.1.1.78.4": UID.SubjectiveRefractionMeasurementsStorage,
"1.2.840.10008.5.1.4.1.1.78.5": UID.VisualAcuityMeasurementsStorage,
"1.2.840.10008.5.1.4.1.1.78.6": UID.SpectaclePrescriptionReportStorage,
"1.2.840.10008.5.1.4.1.1.78.7": UID.OphthalmicAxialMeasurementsStorage,
"1.2.840.10008.5.1.4.1.1.78.8": UID.IntraocularLensCalculationsStorage,
"1.2.840.10008.5.1.4.1.1.79.1": UID.MacularGridThicknessandVolumeReportStorage,
"1.2.840.10008.5.1.4.1.1.80.1": UID.OphthalmicVisualFieldStaticPerimetryMeasurementsStorage,
"1.2.840.10008.5.1.4.1.1.81.1": UID.OphthalmicThicknessMapStorage,
"11.2.840.10008.5.1.4.1.1.82.1": UID.CornealTopographyMapStorage,
"1.2.840.10008.5.1.4.1.1.88.1": UID.TextSRStorage_Trial_Retired,
"1.2.840.10008.5.1.4.1.1.88.2": UID.AudioSRStorage_Trial_Retired,
"1.2.840.10008.5.1.4.1.1.88.3": UID.DetailSRStorage_Trial_Retired,
"1.2.840.10008.5.1.4.1.1.88.4": UID.ComprehensiveSRStorage_Trial_Retired,
"1.2.840.10008.5.1.4.1.1.88.11": UID.BasicTextSRStorage,
"1.2.840.10008.5.1.4.1.1.88.22": UID.EnhancedSRStorage,
"1.2.840.10008.5.1.4.1.1.88.33": UID.ComprehensiveSRStorage,
"1.2.840.10008.5.1.4.1.1.88.34": UID.Comprehensive3DSRStorage,
"1.2.840.10008.5.1.4.1.1.88.40": UID.ProcedureLogStorage,
"1.2.840.10008.5.1.4.1.1.88.50": UID.MammographyCADSRStorage,
"1.2.840.10008.5.1.4.1.1.88.59": UID.KeyObjectSelectionDocumentStorage,
"1.2.840.10008.5.1.4.1.1.88.65": UID.ChestCADSRStorage,
"1.2.840.10008.5.1.4.1.1.88.67": UID.X_RayRadiationDoseSRStorage,
"1.2.840.10008.5.1.4.1.1.88.69": UID.ColonCADSRStorage,
"1.2.840.10008.5.1.4.1.1.88.70": UID.ImplantationPlanSRStorage,
"1.2.840.10008.5.1.4.1.1.104.1": UID.EncapsulatedPDFStorage,
"1.2.840.10008.5.1.4.1.1.104.2": UID.EncapsulatedCDAStorage,
"1.2.840.10008.5.1.4.1.1.128": UID.PositronEmissionTomographyImageStorage,
"1.2.840.10008.5.1.4.1.1.128.1": UID.LegacyConvertedEnhancedPETImageStorage,
"1.2.840.10008.5.1.4.1.1.129": UID.StandalonePETCurveStorage_Retired,
"1.2.840.10008.5.1.4.1.1.130": UID.EnhancedPETImageStorage,
"1.2.840.10008.5.1.4.1.1.131": UID.BasicStructuredDisplayStorage,
"1.2.840.10008.5.1.4.1.1.481.1": UID.RTImageStorage,
"1.2.840.10008.5.1.4.1.1.481.2": UID.RTDoseStorage,
"1.2.840.10008.5.1.4.1.1.481.3": UID.RTStructureSetStorage,
"1.2.840.10008.5.1.4.1.1.481.4": UID.RTBeamsTreatmentRecordStorage,
"1.2.840.10008.5.1.4.1.1.481.5": UID.RTPlanStorage,
"1.2.840.10008.5.1.4.1.1.481.6": UID.RTBrachyTreatmentRecordStorage,
"1.2.840.10008.5.1.4.1.1.481.7": UID.RTTreatmentSummaryRecordStorage,
"1.2.840.10008.5.1.4.1.1.481.8": UID.RTIonPlanStorage,
"1.2.840.10008.5.1.4.1.1.481.9": UID.RTIonBeamsTreatmentRecordStorage,
"1.2.840.10008.5.1.4.1.1.501.1": UID.DICOSCTImageStorage,
"1.2.840.10008.5.1.4.1.1.501.2.1": UID.DICOSDigitalX_RayImageStorage_ForPresentation,
"1.2.840.10008.5.1.4.1.1.501.2.2": UID.DICOSDigitalX_RayImageStorage_ForProcessing,
"1.2.840.10008.5.1.4.1.1.501.3": UID.DICOSThreatDetectionReportStorage,
"1.2.840.10008.5.1.4.1.1.501.4": UID.DICOS2DAITStorage,
"1.2.840.10008.5.1.4.1.1.501.5": UID.DICOS3DAITStorage,
"1.2.840.10008.5.1.4.1.1.501.6": UID.DICOSQuadrupoleResonanceStorage,
"1.2.840.10008.5.1.4.1.1.601.1": UID.EddyCurrentImageStorage,
"1.2.840.10008.5.1.4.1.1.601.2": UID.EddyCurrentMulti_frameImageStorage,
"1.2.840.10008.5.1.4.1.2.1.1": UID.PatientRootQuery_RetrieveInformationModel_FIND,
"1.2.840.10008.5.1.4.1.2.1.2": UID.PatientRootQuery_RetrieveInformationModel_MOVE,
"1.2.840.10008.5.1.4.1.2.1.3": UID.PatientRootQuery_RetrieveInformationModel_GET,
"1.2.840.10008.5.1.4.1.2.2.1": UID.StudyRootQuery_RetrieveInformationModel_FIND,
"1.2.840.10008.5.1.4.1.2.2.2": UID.StudyRootQuery_RetrieveInformationModel_MOVE,
"1.2.840.10008.5.1.4.1.2.2.3": UID.StudyRootQuery_RetrieveInformationModel_GET,
"1.2.840.10008.5.1.4.1.2.3.1": UID.Patient_StudyOnlyQuery_RetrieveInformationModel_FIND_Retired,
"1.2.840.10008.5.1.4.1.2.3.2": UID.Patient_StudyOnlyQuery_RetrieveInformationModel_MOVE_Retired,
"1.2.840.10008.5.1.4.1.2.3.3": UID.Patient_StudyOnlyQuery_RetrieveInformationModel_GET_Retired,
"1.2.840.10008.5.1.4.1.2.4.2": UID.CompositeInstanceRootRetrieve_MOVE,
"1.2.840.10008.5.1.4.1.2.4.3": UID.CompositeInstanceRootRetrieve_GET,
"1.2.840.10008.5.1.4.1.2.5.3": UID.CompositeInstanceRetrieveWithoutBulkData_GET,
"1.2.840.10008.5.1.4.31": UID.ModalityWorklistInformationModel_FIND,
"1.2.840.10008.5.1.4.32.1": UID.GeneralPurposeWorklistInformationModel_FIND_Retired,
"1.2.840.10008.5.1.4.32.2": UID.GeneralPurposeScheduledProcedureStepSOPClass_Retired,
"1.2.840.10008.5.1.4.32.3": UID.GeneralPurposePerformedProcedureStepSOPClass_Retired,
"1.2.840.10008.5.1.4.32": UID.GeneralPurposeWorklistManagementMetaSOPClass_Retired,
"1.2.840.10008.5.1.4.33": UID.InstanceAvailabilityNotificationSOPClass,
"1.2.840.10008.5.1.4.34.1": UID.RTBeamsDeliveryInstructionStorage_Trial_Retired,
"1.2.840.10008.5.1.4.34.2": UID.RTConventionalMachineVerification_Trial_Retired,
"1.2.840.10008.5.1.4.34.3": UID.RTIonMachineVerification_Trial_Retired,
"1.2.840.10008.5.1.4.34.4": UID.UnifiedWorklistandProcedureStepServiceClass_Trial_Retired,
"1.2.840.10008.5.1.4.34.4.1": UID.UnifiedProcedureStep_PushSOPClass_Trial_Retired,
"1.2.840.10008.5.1.4.34.4.2": UID.UnifiedProcedureStep_WatchSOPClass_Trial_Retired,
"1.2.840.10008.5.1.4.34.4.3": UID.UnifiedProcedureStep_PullSOPClass_Trial_Retired,
"1.2.840.10008.5.1.4.34.4.4": UID.UnifiedProcedureStep_EventSOPClass_Trial_Retired,
"1.2.840.10008.5.1.4.34.5": UID.UnifiedWorklistandProcedureStepSOPInstance,
"1.2.840.10008.5.1.4.34.6": UID.UnifiedWorklistandProcedureStepServiceClass,
"1.2.840.10008.5.1.4.34.6.1": UID.UnifiedProcedureStep_PushSOPClass,
"1.2.840.10008.5.1.4.34.6.2": UID.UnifiedProcedureStep_WatchSOPClass,
"1.2.840.10008.5.1.4.34.6.3": UID.UnifiedProcedureStep_PullSOPClass,
"1.2.840.10008.5.1.4.34.6.4": UID.UnifiedProcedureStep_EventSOPClass,
"1.2.840.10008.5.1.4.34.7": UID.RTBeamsDeliveryInstructionStorage,
"1.2.840.10008.5.1.4.34.8": UID.RTConventionalMachineVerification,
"1.2.840.10008.5.1.4.34.9": UID.RTIonMachineVerification,
"1.2.840.10008.5.1.4.37.1": UID.GeneralRelevantPatientInformationQuery,
"1.2.840.10008.5.1.4.37.2": UID.BreastImagingRelevantPatientInformationQuery,
"1.2.840.10008.5.1.4.37.3": UID.CardiacRelevantPatientInformationQuery,
"1.2.840.10008.5.1.4.38.1": UID.HangingProtocolStorage,
"1.2.840.10008.5.1.4.38.2": UID.HangingProtocolInformationModel_FIND,
"1.2.840.10008.5.1.4.38.3": UID.HangingProtocolInformationModel_MOVE,
"1.2.840.10008.5.1.4.38.4": UID.HangingProtocolInformationModel_GET,
"1.2.840.10008.5.1.4.39.1": UID.ColorPaletteStorage,
"1.2.840.10008.5.1.4.39.2": UID.ColorPaletteInformationModel_FIND,
"1.2.840.10008.5.1.4.39.3": UID.ColorPaletteInformationModel_MOVE,
"1.2.840.10008.5.1.4.39.4": UID.ColorPaletteInformationModel_GET,
"1.2.840.10008.5.1.4.41": UID.ProductCharacteristicsQuerySOPClass,
"1.2.840.10008.5.1.4.42": UID.SubstanceApprovalQuerySOPClass,
"1.2.840.10008.5.1.4.43.1": UID.GenericImplantTemplateStorage,
"1.2.840.10008.5.1.4.43.2": UID.GenericImplantTemplateInformationModel_FIND,
"1.2.840.10008.5.1.4.43.3": UID.GenericImplantTemplateInformationModel_MOVE,
"1.2.840.10008.5.1.4.43.4": UID.GenericImplantTemplateInformationModel_GET,
"1.2.840.10008.5.1.4.44.1": UID.ImplantAssemblyTemplateStorage,
"1.2.840.10008.5.1.4.44.2": UID.ImplantAssemblyTemplateInformationModel_FIND,
"1.2.840.10008.5.1.4.44.3": UID.ImplantAssemblyTemplateInformationModel_MOVE,
"1.2.840.10008.5.1.4.44.4": UID.ImplantAssemblyTemplateInformationModel_GET,
"1.2.840.10008.5.1.4.45.1": UID.ImplantTemplateGroupStorage,
"1.2.840.10008.5.1.4.45.2": UID.ImplantTemplateGroupInformationModel_FIND,
"1.2.840.10008.5.1.4.45.3": UID.ImplantTemplateGroupInformationModel_MOVE,
"1.2.840.10008.5.1.4.45.4": UID.ImplantTemplateGroupInformationModel_GET,
"1.2.840.10008.7.1.1": UID.NativeDICOMModel,
"1.2.840.10008.7.1.2": UID.AbstractMulti_DimensionalImageModel,
"1.2.840.10008.15.0.3.1": UID.dicomDeviceName,
"1.2.840.10008.15.0.3.2": UID.dicomDescription,
"1.2.840.10008.15.0.3.3": UID.dicomManufacturer,
"1.2.840.10008.15.0.3.4": UID.dicomManufacturerModelName,
"1.2.840.10008.15.0.3.5": UID.dicomSoftwareVersion,
"1.2.840.10008.15.0.3.6": UID.dicomVendorData,
"1.2.840.10008.15.0.3.7": UID.dicomAETitle,
"1.2.840.10008.15.0.3.8": UID.dicomNetworkConnectionReference,
"1.2.840.10008.15.0.3.9": UID.dicomApplicationCluster,
"1.2.840.10008.15.0.3.10": UID.dicomAssociationInitiator,
"1.2.840.10008.15.0.3.11": UID.dicomAssociationAcceptor,
"1.2.840.10008.15.0.3.12": UID.dicomHostname,
"1.2.840.10008.15.0.3.13": UID.dicomPort,
"1.2.840.10008.15.0.3.14": UID.dicomSOPClass,
"1.2.840.10008.15.0.3.15": UID.dicomTransferRole,
"1.2.840.10008.15.0.3.16": UID.dicomTransferSyntax,
"1.2.840.10008.15.0.3.17": UID.dicomPrimaryDeviceType,
"1.2.840.10008.15.0.3.18": UID.dicomRelatedDeviceReference,
"1.2.840.10008.15.0.3.19": UID.dicomPreferredCalledAETitle,
"1.2.840.10008.15.0.3.20": UID.dicomTLSCyphersuite,
"1.2.840.10008.15.0.3.21": UID.dicomAuthorizedNodeCertificateReference,
"1.2.840.10008.15.0.3.22": UID.dicomThisNodeCertificateReference,
"1.2.840.10008.15.0.3.23": UID.dicomInstalled,
"1.2.840.10008.15.0.3.24": UID.dicomStationName,
"1.2.840.10008.15.0.3.25": UID.dicomDeviceSerialNumber,
"1.2.840.10008.15.0.3.26": UID.dicomInstitutionName,
"1.2.840.10008.15.0.3.27": UID.dicomInstitutionAddress,
"1.2.840.10008.15.0.3.28": UID.dicomInstitutionDepartmentName,
"1.2.840.10008.15.0.3.29": UID.dicomIssuerOfPatientID,
"1.2.840.10008.15.0.3.30": UID.dicomPreferredCallingAETitle,
"1.2.840.10008.15.0.3.31": UID.dicomSupportedCharacterSet,
"1.2.840.10008.15.0.4.1": UID.dicomConfigurationRoot,
"1.2.840.10008.15.0.4.2": UID.dicomDevicesRoot,
"1.2.840.10008.15.0.4.3": UID.dicomUniqueAETitlesRegistryRoot,
"1.2.840.10008.15.0.4.4": UID.dicomDevice,
"1.2.840.10008.15.0.4.5": UID.dicomNetworkAE,
"1.2.840.10008.15.0.4.6": UID.dicomNetworkConnection,
"1.2.840.10008.15.0.4.7": UID.dicomUniqueAETitle,
"1.2.840.10008.15.0.4.8": UID.dicomTransferCapability,
"1.2.840.10008.15.1.1": UID.UniversalCoordinatedTime,
};