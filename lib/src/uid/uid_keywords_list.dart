// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library uid_keywords_list;

/**
 * This library contains compile time constant definitions of "Well Known" [UID]s
 * and there corresponding [String] values.  All names start with '$'.
 */
const List<String> $UID_KeywordsList = const [
    "VerificationSOPClass",
    "ImplicitVRLittleEndianDefaultTransferSyntaxforDICOM",
    "ExplicitVRLittleEndian",
    "DeflatedExplicitVRLittleEndian",
    "ExplicitVRBigEndian_Retired",
    "JPEGBaseline_1DefaultTransferSyntaxforLossyJPEG8BitImageCompression",
    "JPEGExtended_2_4DefaultTransferSyntaxforLossyJPEG12BitImageCompression_4",
    "JPEGExtended_3_5_Retired",
    "JPEGSpectralSelectionNon_Hierarchical_6_8_Retired",
    "JPEGSpectralSelectionNon_Hierarchical_7_9_Retired",
    "JPEGFullProgressionNon_Hierarchical_10_12_Retired",
    "JPEGFullProgressionNon_Hierarchical_11_13_Retired",
    "JPEGLosslessNon_Hierarchical_14",
    "JPEGLosslessNon_Hierarchical_15_Retired",
    "JPEGExtendedHierarchical_16_18_Retired",
    "JPEGExtendedHierarchical_17_19_Retired",
    "JPEGSpectralSelectionHierarchical_20_22_Retired",
    "JPEGSpectralSelectionHierarchical_21_23_Retired",
    "JPEGFullProgressionHierarchical_24_26_Retired",
    "JPEGFullProgressionHierarchical_25_27_Retired",
    "JPEGLosslessHierarchical_28_Retired",
    "JPEGLosslessHierarchical_29_Retired",
    "JPEGLosslessNon_HierarchicalFirst_OrderPrediction_14_1DefaultTransferSyntaxforLosslessJPEGImageCompression",
    "JPEG_LSLosslessImageCompression",
    "JPEG_LSLossyImageCompression",
    "JPEG2000ImageCompressionLosslessOnly",
    "JPEG2000ImageCompression",
    "JPEG2000Part2Multi_componentImageCompressionLosslessOnly",
    "JPEG2000Part2Multi_componentImageCompression",
    "JPIPReferenced",
    "JPIPReferencedDeflate",
    "MPEG2MainProfile_MainLevel",
    "MPEG2MainProfile_HighLevel",
    "MPEG_4AVC_H264HighProfile_Level41",
    "MPEG_4AVC_H264BD_compatibleHighProfile_Level41",
    "RLELossless",
    "RFC2557MIMEencapsulation",
    "XMLEncoding",
    "MediaStorageDirectoryStorage",
    "TalairachBrainAtlasFrameofReference",
    "SPM2T1FrameofReference",
    "SPM2T2FrameofReference",
    "SPM2PDFrameofReference",
    "SPM2EPIFrameofReference",
    "SPM2FILT1FrameofReference",
    "SPM2PETFrameofReference",
    "SPM2TRANSMFrameofReference",
    "SPM2SPECTFrameofReference",
    "SPM2GRAYFrameofReference",
    "SPM2WHITEFrameofReference",
    "SPM2CSFFrameofReference",
    "SPM2BRAINMASKFrameofReference",
    "SPM2AVG305T1FrameofReference",
    "SPM2AVG152T1FrameofReference",
    "SPM2AVG152T2FrameofReference",
    "SPM2AVG152PDFrameofReference",
    "SPM2SINGLESUBJT1FrameofReference",
    "ICBM452T1FrameofReference",
    "ICBMSingleSubjectMRIFrameofReference",
    "HotIronColorPaletteSOPInstance",
    "PETColorPaletteSOPInstance",
    "HotMetalBlueColorPaletteSOPInstance",
    "PET20StepColorPaletteSOPInstance",
    "BasicStudyContentNotificationSOPClass_Retired",
    "StorageCommitmentPushModelSOPClass",
    "StorageCommitmentPushModelSOPInstance",
    "StorageCommitmentPullModelSOPClass_Retired",
    "StorageCommitmentPullModelSOPInstance_Retired",
    "ProceduralEventLoggingSOPClass",
    "ProceduralEventLoggingSOPInstance",
    "SubstanceAdministrationLoggingSOPClass",
    "SubstanceAdministrationLoggingSOPInstance",
    "DICOMUIDRegistry",
    "DICOMControlledTerminology",
    "DICOMApplicationContextName",
    "DetachedPatientManagementSOPClass_Retired",
    "DetachedPatientManagementMetaSOPClass_Retired",
    "DetachedVisitManagementSOPClass_Retired",
    "DetachedStudyManagementSOPClass_Retired",
    "StudyComponentManagementSOPClass_Retired",
    "ModalityPerformedProcedureStepSOPClass",
    "ModalityPerformedProcedureStepRetrieveSOPClass",
    "ModalityPerformedProcedureStepNotificationSOPClass",
    "DetachedResultsManagementSOPClass_Retired",
    "DetachedResultsManagementMetaSOPClass_Retired",
    "DetachedStudyManagementMetaSOPClass_Retired",
    "DetachedInterpretationManagementSOPClass_Retired",
    "StorageServiceClass",
    "BasicFilmSessionSOPClass",
    "BasicFilmBoxSOPClass",
    "BasicGrayscaleImageBoxSOPClass",
    "BasicColorImageBoxSOPClass",
    "ReferencedImageBoxSOPClass_Retired",
    "BasicGrayscalePrintManagementMetaSOPClass",
    "ReferencedGrayscalePrintManagementMetaSOPClass_Retired",
    "PrintJobSOPClass",
    "BasicAnnotationBoxSOPClass",
    "PrinterSOPClass",
    "PrinterConfigurationRetrievalSOPClass",
    "PrinterSOPInstance",
    "PrinterConfigurationRetrievalSOPInstance",
    "BasicColorPrintManagementMetaSOPClass",
    "ReferencedColorPrintManagementMetaSOPClass_Retired",
    "VOILUTBoxSOPClass",
    "PresentationLUTSOPClass",
    "ImageOverlayBoxSOPClass_Retired",
    "BasicPrintImageOverlayBoxSOPClass_Retired",
    "PrintQueueSOPInstance_Retired",
    "PrintQueueManagementSOPClass_Retired",
    "StoredPrintStorageSOPClass_Retired",
    "HardcopyGrayscaleImageStorageSOPClass_Retired",
    "HardcopyColorImageStorageSOPClass_Retired",
    "PullPrintRequestSOPClass_Retired",
    "PullStoredPrintManagementMetaSOPClass_Retired",
    "MediaCreationManagementSOPClassUID",
    "ComputedRadiographyImageStorage",
    "DigitalX_RayImageStorage_ForPresentation",
    "DigitalX_RayImageStorage_ForProcessing",
    "DigitalMammographyX_RayImageStorage_ForPresentation",
    "DigitalMammographyX_RayImageStorage_ForProcessing",
    "DigitalIntra_OralX_RayImageStorage_ForPresentation",
    "DigitalIntra_OralX_RayImageStorage_ForProcessing",
    "CTImageStorage",
    "EnhancedCTImageStorage",
    "LegacyConvertedEnhancedCTImageStorage",
    "UltrasoundMulti_frameImageStorage_Retired",
    "UltrasoundMulti_frameImageStorage",
    "MRImageStorage",
    "EnhancedMRImageStorage",
    "MRSpectroscopyStorage",
    "EnhancedMRColorImageStorage",
    "LegacyConvertedEnhancedMRImageStorage",
    "NuclearMedicineImageStorage_Retired",
    "UltrasoundImageStorage_Retired",
    "UltrasoundImageStorage",
    "EnhancedUSVolumeStorage",
    "SecondaryCaptureImageStorage",
    "Multi_frameSingleBitSecondaryCaptureImageStorage",
    "Multi_frameGrayscaleByteSecondaryCaptureImageStorage",
    "Multi_frameGrayscaleWordSecondaryCaptureImageStorage",
    "Multi_frameTrueColorSecondaryCaptureImageStorage",
    "StandaloneOverlayStorage_Retired",
    "StandaloneCurveStorage_Retired",
    "WaveformStorage_Trial_Retired",
    "twelve_lead_12ECGWaveformStorage",
    "GeneralECGWaveformStorage",
    "AmbulatoryECGWaveformStorage",
    "HemodynamicWaveformStorage",
    "CardiacElectrophysiologyWaveformStorage",
    "BasicVoiceAudioWaveformStorage",
    "GeneralAudioWaveformStorage",
    "ArterialPulseWaveformStorage",
    "RespiratoryWaveformStorage",
    "StandaloneModalityLUTStorage_Retired",
    "StandaloneVOILUTStorage_Retired",
    "GrayscaleSoftcopyPresentationStateStorageSOPClass",
    "ColorSoftcopyPresentationStateStorageSOPClass",
    "Pseudo_ColorSoftcopyPresentationStateStorageSOPClass",
    "BlendingSoftcopyPresentationStateStorageSOPClass",
    "XA_XRFGrayscaleSoftcopyPresentationStateStorage",
    "X_RayAngiographicImageStorage",
    "EnhancedXAImageStorage",
    "X_RayRadiofluoroscopicImageStorage",
    "EnhancedXRFImageStorage",
    "X_RayAngiographicBi_PlaneImageStorage_Retired",
    "X_Ray3DAngiographicImageStorage",
    "X_Ray3DCraniofacialImageStorage",
    "BreastTomosynthesisImageStorage",
    "IntravascularOpticalCoherenceTomographyImageStorage_ForPresentation",
    "IntravascularOpticalCoherenceTomographyImageStorage_ForProcessing",
    "NuclearMedicineImageStorage",
    "RawDataStorage",
    "SpatialRegistrationStorage",
    "SpatialFiducialsStorage",
    "DeformableSpatialRegistrationStorage",
    "SegmentationStorage",
    "SurfaceSegmentationStorage",
    "RealWorldValueMappingStorage",
    "SurfaceScanMeshStorage",
    "SurfaceScanPointCloudStorage",
    "VLImageStorage_Trial_Retired",
    "VLMulti_frameImageStorage_Trial_Retired",
    "VLEndoscopicImageStorage",
    "VideoEndoscopicImageStorage",
    "VLMicroscopicImageStorage",
    "VideoMicroscopicImageStorage",
    "VLSlide_CoordinatesMicroscopicImageStorage",
    "VLPhotographicImageStorage",
    "VideoPhotographicImageStorage",
    "OphthalmicPhotography8BitImageStorage",
    "OphthalmicPhotography16BitImageStorage",
    "StereometricRelationshipStorage",
    "OphthalmicTomographyImageStorage",
    "VLWholeSlideMicroscopyImageStorage",
    "LensometryMeasurementsStorage",
    "AutorefractionMeasurementsStorage",
    "KeratometryMeasurementsStorage",
    "SubjectiveRefractionMeasurementsStorage",
    "VisualAcuityMeasurementsStorage",
    "SpectaclePrescriptionReportStorage",
    "OphthalmicAxialMeasurementsStorage",
    "IntraocularLensCalculationsStorage",
    "MacularGridThicknessandVolumeReportStorage",
    "OphthalmicVisualFieldStaticPerimetryMeasurementsStorage",
    "OphthalmicThicknessMapStorage",
    "CornealTopographyMapStorage",
    "TextSRStorage_Trial_Retired",
    "AudioSRStorage_Trial_Retired",
    "DetailSRStorage_Trial_Retired",
    "ComprehensiveSRStorage_Trial_Retired",
    "BasicTextSRStorage",
    "EnhancedSRStorage",
    "ComprehensiveSRStorage",
    "Comprehensive3DSRStorage",
    "ProcedureLogStorage",
    "MammographyCADSRStorage",
    "KeyObjectSelectionDocumentStorage",
    "ChestCADSRStorage",
    "X_RayRadiationDoseSRStorage",
    "ColonCADSRStorage",
    "ImplantationPlanSRStorage",
    "EncapsulatedPDFStorage",
    "EncapsulatedCDAStorage",
    "PositronEmissionTomographyImageStorage",
    "LegacyConvertedEnhancedPETImageStorage",
    "StandalonePETCurveStorage_Retired",
    "EnhancedPETImageStorage",
    "BasicStructuredDisplayStorage",
    "RTImageStorage",
    "RTDoseStorage",
    "RTStructureSetStorage",
    "RTBeamsTreatmentRecordStorage",
    "RTPlanStorage",
    "RTBrachyTreatmentRecordStorage",
    "RTTreatmentSummaryRecordStorage",
    "RTIonPlanStorage",
    "RTIonBeamsTreatmentRecordStorage",
    "DICOSCTImageStorage",
    "DICOSDigitalX_RayImageStorage_ForPresentation",
    "DICOSDigitalX_RayImageStorage_ForProcessing",
    "DICOSThreatDetectionReportStorage",
    "DICOS2DAITStorage",
    "DICOS3DAITStorage",
    "DICOSQuadrupoleResonanceStorage",
    "EddyCurrentImageStorage",
    "EddyCurrentMulti_frameImageStorage",
    "PatientRootQuery_RetrieveInformationModel_FIND",
    "PatientRootQuery_RetrieveInformationModel_MOVE",
    "PatientRootQuery_RetrieveInformationModel_GET",
    "StudyRootQuery_RetrieveInformationModel_FIND",
    "StudyRootQuery_RetrieveInformationModel_MOVE",
    "StudyRootQuery_RetrieveInformationModel_GET",
    "Patient_StudyOnlyQuery_RetrieveInformationModel_FIND_Retired",
    "Patient_StudyOnlyQuery_RetrieveInformationModel_MOVE_Retired",
    "Patient_StudyOnlyQuery_RetrieveInformationModel_GET_Retired",
    "CompositeInstanceRootRetrieve_MOVE",
    "CompositeInstanceRootRetrieve_GET",
    "CompositeInstanceRetrieveWithoutBulkData_GET",
    "ModalityWorklistInformationModel_FIND",
    "GeneralPurposeWorklistInformationModel_FIND_Retired",
    "GeneralPurposeScheduledProcedureStepSOPClass_Retired",
    "GeneralPurposePerformedProcedureStepSOPClass_Retired",
    "GeneralPurposeWorklistManagementMetaSOPClass_Retired",
    "InstanceAvailabilityNotificationSOPClass",
    "RTBeamsDeliveryInstructionStorage_Trial_Retired",
    "RTConventionalMachineVerification_Trial_Retired",
    "RTIonMachineVerification_Trial_Retired",
    "UnifiedWorklistandProcedureStepServiceClass_Trial_Retired",
    "UnifiedProcedureStep_PushSOPClass_Trial_Retired",
    "UnifiedProcedureStep_WatchSOPClass_Trial_Retired",
    "UnifiedProcedureStep_PullSOPClass_Trial_Retired",
    "UnifiedProcedureStep_EventSOPClass_Trial_Retired",
    "UnifiedWorklistandProcedureStepSOPInstance",
    "UnifiedWorklistandProcedureStepServiceClass",
    "UnifiedProcedureStep_PushSOPClass",
    "UnifiedProcedureStep_WatchSOPClass",
    "UnifiedProcedureStep_PullSOPClass",
    "UnifiedProcedureStep_EventSOPClass",
    "RTBeamsDeliveryInstructionStorage",
    "RTConventionalMachineVerification",
    "RTIonMachineVerification",
    "GeneralRelevantPatientInformationQuery",
    "BreastImagingRelevantPatientInformationQuery",
    "CardiacRelevantPatientInformationQuery",
    "HangingProtocolStorage",
    "HangingProtocolInformationModel_FIND",
    "HangingProtocolInformationModel_MOVE",
    "HangingProtocolInformationModel_GET",
    "ColorPaletteStorage",
    "ColorPaletteInformationModel_FIND",
    "ColorPaletteInformationModel_MOVE",
    "ColorPaletteInformationModel_GET",
    "ProductCharacteristicsQuerySOPClass",
    "SubstanceApprovalQuerySOPClass",
    "GenericImplantTemplateStorage",
    "GenericImplantTemplateInformationModel_FIND",
    "GenericImplantTemplateInformationModel_MOVE",
    "GenericImplantTemplateInformationModel_GET",
    "ImplantAssemblyTemplateStorage",
    "ImplantAssemblyTemplateInformationModel_FIND",
    "ImplantAssemblyTemplateInformationModel_MOVE",
    "ImplantAssemblyTemplateInformationModel_GET",
    "ImplantTemplateGroupStorage",
    "ImplantTemplateGroupInformationModel_FIND",
    "ImplantTemplateGroupInformationModel_MOVE",
    "ImplantTemplateGroupInformationModel_GET",
    "NativeDICOMModel",
    "AbstractMulti_DimensionalImageModel",
    "dicomDeviceName",
    "dicomDescription",
    "dicomManufacturer",
    "dicomManufacturerModelName",
    "dicomSoftwareVersion",
    "dicomVendorData",
    "dicomAETitle",
    "dicomNetworkConnectionReference",
    "dicomApplicationCluster",
    "dicomAssociationInitiator",
    "dicomAssociationAcceptor",
    "dicomHostname",
    "dicomPort",
    "dicomSOPClass",
    "dicomTransferRole",
    "dicomTransferSyntax",
    "dicomPrimaryDeviceType",
    "dicomRelatedDeviceReference",
    "dicomPreferredCalledAETitle",
    "dicomTLSCyphersuite",
    "dicomAuthorizedNodeCertificateReference",
    "dicomThisNodeCertificateReference",
    "dicomInstalled",
    "dicomStationName",
    "dicomDeviceSerialNumber",
    "dicomInstitutionName",
    "dicomInstitutionAddress",
    "dicomInstitutionDepartmentName",
    "dicomIssuerOfPatientID",
    "dicomPreferredCallingAETitle",
    "dicomSupportedCharacterSet",
    "dicomConfigurationRoot",
    "dicomDevicesRoot",
    "dicomUniqueAETitlesRegistryRoot",
    "dicomDevice",
    "dicomNetworkAE",
    "dicomNetworkConnection",
    "dicomUniqueAETitle",
    "dicomTransferCapability",
    "UniversalCoordinatedTime"];
