// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library uid_type;

/**
 * The types of Well Known UIDs
 */
class UIDType {
  final int    index;
  final String name;

  const UIDType(this.index, this.name);

  static const SOPClass                        = const UIDType(0,  "SOPClass");
  static const TransferSyntax                  = const UIDType(1,  "TransferSyntax");
  static const FrameOfReference                = const UIDType(2,  "FrameOfReference");
  static const SOPInstance                     = const UIDType(3,  "SOPInstance");
  static const DicomCodingScheme               = const UIDType(4,  "DicomCodingScheme");
  static const MetaSOPClass                    = const UIDType(5,  "MetaSOPClass");
  static const ServiceClass                    = const UIDType(6,  "ServiceClass");
  static const CodingScheme                    = const UIDType(7,  "CodingScheme");
  static const ApplicationContexName           = const UIDType(8,  "ApplicationContextName");
  static const PrinterSOPInstance              = const UIDType(9,  "PrinterSOPInstance");
  static const PrintQueueSOPInstance           = const UIDType(10, "PrintQueueSOPInstance");
  static const QueryRetrieve                   = const UIDType(11, "QueryRetrieve");
  static const ApplicationHostingModel         = const UIDType(12, "ApplicationHostingModel");
  static const LDAP_OID                        = const UIDType(13, "LDAP_OID");
  static const SynchronizationFrameOfReference = const UIDType(14, "SynchronizationFrameOfReference");
  static const Generated                       = const UIDType(15, "Generated");
  static const Constructed                     = const UIDType(16, "Constructed");

  static const Unknown                         = const UIDType(99, "Unknown");
}