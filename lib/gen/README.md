This directory <dictionary/lib/gen> contains programs and tables that are used to
generate DICOM data definitions (and possibly validators) for Tags, VRs, VMs, 
Data Elements, Macros, Modules, IODs, etc.

The generators read JSON objects that define the DICOM data type and it's values.

Each data type will have its own subdirectory 'gen/{type}' which will contain at a 
minimum the following files:
  * {type}_def.json
  * {type}_generate_defs_{lang}.dart
  * {type}_generate_lookup_{lang}.dart
  * {type}_generate_get_name_{lang}.dart
  * {type}_generate_class_{lang}.dart
  
The JSON tables containing the definitions have the format: {class}.json.
For example:
  * tag.json
  * vm.json
  * vm.json
Generator filenames have the following format: {class}_generate.{lang}


//TODO improve this documentation
The following general rules apply to the files generated:
  1. To the extent possible, all files generated contain --compile time constants--.
  2. There is usually a library that contains a set of names defined by a class and 
     their values.  These files are named: {class}_definitions.{lang}.
  3. There is usually a library that defines a Map called {class}Lookup["name"] = value;