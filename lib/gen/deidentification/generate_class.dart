// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

/// generate_class

import 'dart:io';
import './deid_class_table.dart';

/**
 * This program generates the DeID class using the 'deid.json' file that was created
 * from the DICOM table in PS3.15 Appendix E.
 */

void main() {
  String s;
  File   output;

  String jsonFilename        = './deid.json';
  String classFilename       = 'c:/MICA/git/dcmid/toolkit/lib/deid.dart';

  DeIdClassTable table = DeIdClassTable.read(jsonFilename);

  // Write Tags class
  s = table.deIdClassString;
  output = new File(classFilename);
  output.writeAsStringSync(s);

  print('Done');
}