De-Identification Issues

This file documents problems encountered when converting PS3.5 Appendix E Table E.1-1.

  1) The slashes between the action types, e.g. X/D, must be removed.
  2) The table contains attribute names rather than keywords.  This means that all 
     spaces (" ") and apostrophies ("'") must be removed from the names in column 1.
  3) The Y/N entries need to be converted to true/false.
  4) The empty cells need to be replaced with the string 'NA'.