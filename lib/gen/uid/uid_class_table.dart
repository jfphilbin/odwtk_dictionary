// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library constant_class_table;

import 'dart:convert';
import 'dart:io';

//TODO change UIDType from String to UIDType
class UIDClassTable {
  static const int   KEYWORD_OFFSET = 0;
  static const int   TAG_OFFSET = 1;
  String             className;
  int                fieldCount;
  List<String>       fieldTypes;
  List<String>       fieldNames;
  List<List<String>> values;

  UIDClassTable(this.className, this.fieldCount, this.fieldTypes, this.fieldNames, this.values);

  //TODO Document
  String commaSeparatedString(String prefix, List args, String suffix, {last: false}) {
    String s = "";
    int end = args.length - 1;
    for(int i = 0; i < end; i++) {
      s += '$prefix${args[i]}$suffix';
    }
    s += '$prefix${args[end]}';
    s += (last == false) ? "" : suffix;
    return s;
  }

  String get fieldsString {
    String s = "";
    for(int i = 1; i < fieldCount; i++) {
      s += "  final ${fieldTypes[i]} ${fieldNames[i]};\n";
    }
    return s;
  }

  String get constructorString {
    String args = "";
    List argList;
    argList = fieldNames.getRange(1, fieldNames.length).toList();
    args = commaSeparatedString('this.', argList, ", ");
    return '  const $className.defined($args);';
  }

  // Replacement strings to create codeString
  String root = r"$root";
  String matcher = r"'$s: ${match.groups([0,1,2,3])}'";
  String uidString = r"$uid (type=$type, name=$name)";
  String get codeString => """
  UID(this.uid, [this.type="Unknown", this.name="", this.link="", this.isRetired=false]);
  UID.generated(this.uid, this.type, [this.name="", this.link="", this.isRetired=false]);

  /**
   * [UID root] for [UID]s created using [UUID]s (Universally Unique Identifiers) generated
   * in accordance with Rec. ITU-T X.667 | ISO/IEC 9834-8.
   * See <http://www.oid-info.com/get/2.25>
   */
  static const String UUID_URI_ROOT_STRING = "2.25.";

  /**
   * Returns a [UID] generated from a random [UUID].
   */
  static UID randomUID() {
    Uuid uuid = new Uuid();
    String s = UUID_URI_ROOT_STRING + uuid.toString();
    //TODO make 2nd arg be UIDType
    return new UID.generated(s, 'Generated');
  }

  //TODO needed?
  static UID createUID() => randomUID();

  //TODO not used - is it needed?
  static String checkRoot(String root) {
    if (root.length > 24) error("root length > 24");
    if (!isValidUIDString(root)) error('invalid UID root: $root');
    return root;
  }

  static bool isValid(UID uid) {
    return isValidUIDString(uid.uid);
  }

  static final RegExp UID_PATTERN = new RegExp(r"[012]((\\.0)|(\\.[1-9]\\d*))+");

  static bool isValidUIDString(String s) {
    if (s.length > 64) return false;
    Match match = UID_PATTERN.matchAsPrefix(s);
    //if (match != null) print($matcher);
    if (match == null) return false;
    if (match.group(0) == s) return true;
    return false;
  }

  Uint8List toUint8List(String s) => s.codeUnits;
  
  String debug() => "UID: $uidString";

  String toString() => uid;
""";

  // Create a [String] that corresponds to all the [const] members of the class
  String get valuesString {
    String valuesString = "";
    String tableString = "";
    for(int i = 0; i < values.length; i++) {
    //for(int i = 0; i < 100; i++) {
      List v = values[i];
      //print(v);
      String keyword = v[0];
      String args = commaSeparatedString("", v.sublist(1), ", ");
      String table = commaSeparatedString("", v, "; ");
      //print(args);
      valuesString += '  static const $keyword =\n      const $className.defined($args);\n';
      tableString += table + "\n";
    }
    File out = new File("generated_uid.csv");
    out.writeAsStringSync(tableString);
    return valuesString;
  }

  bool testing = false;
  String get imports => (testing) ?
"""
import '../../../core/lib/error.dart';
import '../../../core/lib/uuid.dart';
""":
"""
import 'package:core/error.dart';
import 'package:core/UIDType.dart';
import 'package:core/uuid.dart';
""";

  String get UIDClassString => """
// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library ${className.toLowerCase()};

import 'dart:typed_data';
$imports

// *** This file generated by '../uid/generate_uid_class.dart' ***
// ----------------------------------------------------------

// DICOM UIDs defined in PS3.6 Appendix A

class $className {
$fieldsString
$constructorString
$codeString
//*****   Constant Values   *****  
$valuesString
}
""";

  static UIDClassTable read(String filename) {
    File file = new File(filename);
    String s = file.readAsStringSync();
    Map m = JSON.decode(s);
    return new UIDClassTable(m["className"],
                              m["fieldCount"],
                              m["fieldTypes"],
                              m["fieldNames"],
                              m["values"]);

  }

  static void write(File file, UIDClassTable table) {
    String s = JSON.encode(table);
    file.writeAsStringSync(s);
  }

}