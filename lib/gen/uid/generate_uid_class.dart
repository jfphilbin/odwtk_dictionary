// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

/// generate_class

import 'dart:io';
import './uid_class_table.dart';

/**
 * This program generates the UID class using the 'uid_table.json' file that was created
 * from the DICOM table in PS3.6 Appendix A.
 */

//TODO change UIDType from String to UIDType

void main() {
  String s;
  File   output;

  String jsonFilename        = './uid_table.json';
  //TODO make this rely on the "Testing" variable
  String classFilename       = './uid.dart';

  UIDClassTable table = UIDClassTable.read(jsonFilename);
  s = table.UIDClassString;
  output = new File(classFilename);
  output.writeAsStringSync(s);

  print('Done');
}