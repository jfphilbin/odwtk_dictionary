

List findAndAppendNumbers(String s, int start) {
  print(s.length);
  String test = s.substring(start, start + 9);
  print(test);
  if (test == "(Retired)") {
    // keep
    return [9, "_Retired"];
  }
  String result = "";
  int    zero = "0".codeUnitAt(0);
  int    nine = "9".codeUnitAt(0);
  bool   mode = false;
  for(int i = start; i < s.length; i++) {
    String char = s[i];
    if (char == ")") return [i, result];
    int ascii = char.codeUnitAt(0);
    if (ascii >= zero && ascii <= nine) {
      if (mode == false) {
        result += "_";
        mode = true;
      }
      result += char;
    } else {
      if (mode == true) mode = false;
    }
  }
  return [s.length, result];
}

main() {
  print(findAndAppendNumbers("(Retired)", 0));
  print(findAndAppendNumbers("(Retired)", 0));
  print(findAndAppendNumbers("This(Retired)", 4));
}