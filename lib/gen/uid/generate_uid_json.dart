// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

/// generate_json

import 'dart:convert';
import 'dart:io';

/**
 * This program is used to generate dart classes from the DICOM UID table in PS3.6
 * Appendix A. It reads the .csv file, converts it to a json data structure, and then
 * writes it out to a .json file.
 * These *.json files are used to generate a Dart compile time constant class.
 */

List findAndAppendNumbers(String s, int start) {
  if (s.substring(start, start + 9) == "(Retired)") {
    // keep the retired field
    return [start + 8, "_Retired"];
  }
  String result = "";
  int    zero = "0".codeUnitAt(0);
  int    nine = "9".codeUnitAt(0);
  bool   mode = false;
  for(int i = start; i < s.length; i++) {
    String char = s[i];
    if (char == ")") return [i, result];
    int ascii = char.codeUnitAt(0);
    if (ascii >= zero && ascii <= nine) {
      if (mode == false) {
        result += "_";
        mode = true;
      }
      result += char;
    } else {
      if (mode == true) mode = false;
    }
  }
  return [s.length, result];
}

// Utilities
// TODO move to generators.dart library
String cleanKeyword(String s) {
  // Remove all spaces and apsotrophies
  String result = "";
  String tmp = "";
  bool doPrint = false;
  for(int i = 0; i < s.length; i++) {
    tmp = s[i];
    if (tmp == " " || tmp == "'" || tmp == ":" || tmp == "," || tmp == ".") {
      doPrint = true;
      continue;
    } else if (tmp == "(") {
      List list = findAndAppendNumbers(s, i);
      i = list[0];
      result += list[1];
      doPrint = true;
      continue;
    } else if (tmp == "&" || tmp == "-" || tmp == "@" || tmp == "\\" || tmp == "/") {
      tmp = '_';
      doPrint = true;
    }
    result += tmp;
  }
  if (doPrint) {
    print(s);
    print('>$result');
    doPrint = false;
  }
  return result;
}

String removeSlashes(String s) {
  String result = "";
  String tmp = "";
  for(int i = 0; i < s.length; i++) {
    tmp = s[i];
    if (tmp == "/") continue;
    result += tmp;
  }
  return result;
}

String convertY_N(String value) {
  if (value == 'N') {
    return 'false';
  } else if (value == 'Y') {
    return 'true';
  } else if (value == "") {
    return 'NA';
  } else {
    throw new Error();
  }
}

String convertDeIdActionCodes(String value) {
  if (value == "") {
    return 'NA';
  } else if (value.length > 1) {
    return removeSlashes(value);
  } else {
    return value;
  }
}

String arrayToString(List data) {
  String s = '[\n';
  for (int i = 0; i < data.length; i++) {
    s = s + "    " + data[i].toString() + "\n";
  }
  return s + '  ]';
}

List lineToList(String line) {
  void addSlice(List l, int start, int end) {
    String s = line.substring(start, end).trim();
    if ((s[0] == '"') && (s[s.length - 1] == '"')) {
      s = s.substring(1, s.length - 1);
      print(s);
    }
    l.add(s);
  }

  String char;
  List   list = new List();
  int    length = line.length;
  int    start  = 0;
  for(int i = 0; i < length; i++){
    char = line[i];
    if (char == ",") {
      addSlice(list, start, i);
      start = i + 1;

    } else if (char == '"') {
      for(int j = i + 1; j < line.length; j++) {
        char = line[j];
        if (char == '"') {
          i = j;
          break;
        }
      }
    }
  }
  if (start < length) {
    addSlice(list, start, length);
  } else list.add("");
  return list;
}

/**
 * Take a list [uid, name, isRetired, type, link]
 * and convert it to [keyword, uid, type, isRetired, name, link]
 * where keyword and type have all whitespace and "'" characters removed.
 */
List listToNewListWithKeyword(List oList) {
  List nList = new List(6);
  nList[0] = cleanKeyword(oList[1]);
  nList[1] = '"' + oList[0] + '"';
  nList[2] = '"' + cleanKeyword(oList[3]) + '"';
  nList[3] = oList[2].toLowerCase();
  nList[4] = '"' + oList[1] + '"';
  nList[5] = '"' + oList[4] + '"';
  print(nList);

  return nList;
}

void main() {
  File input = new File("./uid_table.csv");
  File output = new File('./uid_table.json');

  List<String> lines = input.readAsLinesSync(encoding: SYSTEM_ENCODING);
  // Get the Headers
  String className = lines[0].trim();
  int fieldCount = int.parse(lines[1].trim());
  List fieldTypes = lines[2].trim().split(',');
  List fieldNames = lines[3].trim().split(',');
  //***** Fixing up table ******
  fieldCount = 6;
  fieldTypes = ["String", "String", "String", "bool", "String", "String"];
  fieldNames = ["keyword", "uid", "type", "isRetired", "name", "link"];
  //***** End of Fix ******
  List values = new List(lines.length - 4);
  for (int i = 4; i < lines.length; i++) {
    //List row = lines[i].trim().split(',');
    List row = lineToList(lines[i]);
    //**** Fixup code to change the table contents *****
    row = listToNewListWithKeyword(row);
    //**** End of Fix *****
    if (fieldCount != row.length) throw new Error();
    values[i - 4] = row;
  }

  Map table = new Map();
  table["className"] = className;
  // +1 on next line because we added a new field called 'dcmFmt'.
  table["fieldCount"] = fieldCount;
  table["fieldTypes"] = fieldTypes;
  table["fieldNames"] = fieldNames;
  table["values"] = values;

  print(table);

  String jsonString = '''
{"className": "$className",
 "fieldCount": ${fieldTypes.length}, 
 "fieldTypes": $fieldTypes,
 "fieldNames": $fieldNames,  
 "values": ${arrayToString(values)}
}''';

  //print(jsonString);

  JsonEncoder encoder = new JsonEncoder.withIndent("  ");

  String json = encoder.convert(table);
  output.writeAsStringSync(json);

}


