
library tag_table_template;

// A ClassDataMap file format
Map tagDataMap = {
    'MapType'     : "ClassMap",
                  'description' : "description",
                  'className'   : 'classname',
                  'fieldTypes'  : ['fieldTypes'],
                  'memberData'  : [['members']],
                  'lookupType'   : 'Map'
                };

Map exampleTagDataMap = {
  "MapType"     : "ClassMap",
  "description" : "a string describing the class being created",
  "className"   : "VM",
  "fieldNames"  : ["name", "min", "max", "width"],
  "fieldTypes"  : ["String", "int", "int", "int"],
  "memberData"  : [["VM_1",      1,     1,     1],
                   ["VM_1_n",    1,    -1,     1],
                   ["VM_n",      0,    -1,     1],
                   ["VM_2_2n",   2,    -1,     2]],
  "lookupType"  : "Map"
  };