// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library value_representation;

//import 'package:toolkit/bit_list.dart';
import 'package:toolkit/ascii_charset.dart';
import 'package:toolkit/ascii_constants.dart';

/**
 * DICOM Value Representations
 */
class VR {
  final fieldDefs = const
    [[final, String,   symbol,         'TODO should this be a String or Symbol?'],
     [final, int,       code,          'The int encodeing of the two letter name'],
     [final, int,       vfBytes,        ' The number of bytes in the Length of Value Field'],
     [final, int,       paddingByte,    ' Either ASCII null (0) or space (32)'],
  [final, int,       maxLength,      ' The maximum length of a single value'],
  [final, bool,      multipleValues, ' Can the Value Field contain multiple values'],
  [final, bool,      inlineBinary,   ' Can this VR be inlined'],
  [final, AsciiCharset,   charset,   ' BitList of length 128 with legal chars being set true'],
  [final, String,    trimWhitespace, ' Trim leading and trailing or just trailing'],
  [final, String,    name,            'a string']];

  // A lookup table for VR codes.
  static final List<VR> _lookupTable = new List<VR>(0x5554 - 0x4145 + 1);

  const VR(this.symbol, this.code, this.vfBytes, this.paddingByte, this.maxLength, this.multipleValues,
           this.inlineBinary, this.charset, this.trimWhitespace, this.name);

  // In the table below 32 = ' ' and 0 = ascii null
  //          Symbol        String  Code Size Pad Len    MV     InLine Charset Whitspce Name
  static const AE = const VR('AE', 0x4145, 2, 32, 16,    true,  false, Ascii.AETITLE,    "trim",     "AETitle");
  static const AS = const VR('AS', 0x4153, 2, 32, 4,     false, false, null,       null,       "Age");
  static const AT = const VR('AT', 0x4154, 2,  0, 4,     true,  false, null,       null,       "Tag");
  static const BR = const VR('BR', 0x4252, 2,  0, -1,    true,  true,  null,       null,       "BDReference");
  static const CS = const VR('CS', 0x4353, 2, 32, 16,    true,  false, Ascii.CODE_STRING, "trim",     "CodeString");
  static const DA = const VR('DA', 0x4441, 2, 32, 8,     true,  false, Ascii.DATE,       null,       "Date");
  static const DS = const VR('DS', 0x4453, 2, 32, 16,    true,  false, Ascii.DECIMAL,    "trim",     "Decimal");
  static const DT = const VR('DT', 0x4454, 2, 32, 26,    true,  false, Ascii.DATE_TIME,   "trailing", "DataTime");
  static const FD = const VR('FD', 0x4644, 2,  0, 8,     true,  false, null,       null,       "Float64");
  static const FL = const VR('FL', 0x464c, 2,  0, 4,     true,  false, null,       null,       "Float32");
  static const IS = const VR('IS', 0x4953, 2, 32, 12,    true,  false, Ascii.INTEGER,    "trim",     "Integer");
  static const LO = const VR('LO', 0x4c4f, 2, 32, 64,    true,  false, Ascii.STRING,     "trim",     "LongString");
  static const LT = const VR('LT', 0x4c54, 2, 32, 10240, false, false, Ascii.TEXT,       "trailing", "LongText");
  static const OB = const VR('OB', 0x4f42, 4,  0, 1,     false, true,  null,       null,       "OtherByte");
  static const OD = const VR('OD', 0x4f42, 4,  0, 8,     false, true,  null,       null,       "OtherDouble");
  static const OF = const VR('OF', 0x4f46, 4,  0, 4,     false, true,  null,       null,       "OtherFloat");
  static const OW = const VR('OW', 0x4f57, 4,  0, 2,     false, true,  null,       null,       "OtherWord");
  static const PN = const VR('PN', 0x504e, 2, 32, 64,    true,  false, Ascii.PERSON_NAME, "trim",     "PersonName");
  static const SH = const VR('SH', 0x5348, 2, 32, 16,    true,  false, Ascii.STRING,     "trim",     "ShortString");
  static const SL = const VR('SL', 0x534c, 2,  0, 4,     true,  false, null,       null,       "Int32");
  static const SQ = const VR('SQ', 0x5351, 4,  0, -1,    false,  false, null,       null,       "Sequence");
  static const SS = const VR('SS', 0x5353, 2,  0, 2,     true,  false, null,       null,       "Int16");
  static const ST = const VR('ST', 0x5354, 2, 32, 1024,  false, false, Ascii.TEXT,       "trailing", "ShortText");
  static const TM = const VR('TM', 0x544d, 2, 32, 16,    true,  false, Ascii.TIME,   "trailing", "Time");
  static const UI = const VR('UI', 0x5549, 2,  0, 64,    true,  false, null,       null,       "Uid");
  static const UL = const VR('UL', 0x554c, 2,  0, 4,     true,  false, null,       null,       "Uint32");
  static const UN = const VR('UN', 0x554e, 4,  0, -1,    true,  true,  null,       null,       "Unknown");
  static const UR = const VR('UR', 0x5552, 4, 32, -1,    false, false, Ascii.URI,        "trim",     "URI");
  static const US = const VR('US', 0x5553, 2,  0, 2,     true,  false, null,       null,       "Uint16");
  static const UT = const VR('UT', 0x5554, 4, 32, -1,    false, false, Ascii.TEXT,       "trailing", "UText");

  static VR valueOf(int vrCode) {
    VR vr = _lookupTable[vrCode - 0x4145];
    if (vr == null) throw new Exception("Bad vrCode = $vrCode");
    return vr;
  }
  //TODO why not use a map?
  static stringToVR(String s) {
    switch (s) {
      case "AE": return VR.AE;
      case "AS": return VR.AS;
      case "AT": return VR.AT;
      case "BR": return VR.BR;
      case "CS": return VR.CS;
      case "DA": return VR.DA;
      case "DS": return VR.DS;
      case "DT": return VR.DT;
      case "FD": return VR.FD;
      case "FL": return VR.FL;
      case "IS": return VR.IS;
      case "LO": return VR.LO;
      case "LT": return VR.LT;
      case "OB": return VR.OB;
      case "OD": return VR.OD;
      case "OF": return VR.OF;
      case "OW": return VR.OW;
      case "PN": return VR.PN;
      case "SH": return VR.SH;
      case "SL": return VR.SL;
      case "SQ": return VR.SQ;
      case "SS": return VR.SS;
      case "ST": return VR.ST;
      case "TM": return VR.TM;
      case "UI": return VR.UI;
      case "UL": return VR.UL;
      case "UN": return VR.UN;
      case "UR": return VR.UR;
      case "US": return VR.US;
      case "UT": return VR.UT;
    }
  }

  bool get isLarge => vfBytes == 4;

  String toString() {
    return "VR.$symbol(code=$code(0x${code.toRadixString(16)}), size=$vfBytes, pad=$paddingByte, inline=$inlineBinary, name=$name)";
  }

  /**
   * Initializes the VR lookup table.  This function must be called before VR static members
   * can be accessed.
   */
  static bool initializeVR() {
    VR._lookupTable[0x4145 - 0x4145] = VR.AE;
    VR._lookupTable[0x4153 - 0x4145] = VR.AS;
    VR._lookupTable[0x4154 - 0x4145] = VR.AT;
    VR._lookupTable[0x4252 - 0x4145] = VR.BR;
    VR._lookupTable[0x4353 - 0x4145] = VR.CS;
    VR._lookupTable[0x4441 - 0x4145] = VR.DA;
    VR._lookupTable[0x4453 - 0x4145] = VR.DS;
    VR._lookupTable[0x4454 - 0x4145] = VR.DT;
    VR._lookupTable[0x4644 - 0x4145] = VR.FD;
    VR._lookupTable[0x464c - 0x4145] = VR.FL;
    VR._lookupTable[0x4953 - 0x4145] = VR.IS;
    VR._lookupTable[0x4c4f - 0x4145] = VR.LO;
    VR._lookupTable[0x4c54 - 0x4145] = VR.LT;
    VR._lookupTable[0x4f42 - 0x4145] = VR.OB;
    VR._lookupTable[0x4f44 - 0x4145] = VR.OD;
    VR._lookupTable[0x4f46 - 0x4145] = VR.OF;
    VR._lookupTable[0x4f57 - 0x4145] = VR.OW;
    VR._lookupTable[0x504e - 0x4145] = VR.PN;
    VR._lookupTable[0x5348 - 0x4145] = VR.SH;
    VR._lookupTable[0x534c - 0x4145] = VR.SL;
    VR._lookupTable[0x5351 - 0x4145] = VR.SQ;
    VR._lookupTable[0x5353 - 0x4145] = VR.SS;
    VR._lookupTable[0x5354 - 0x4145] = VR.ST;
    VR._lookupTable[0x544d - 0x4145] = VR.TM;
    VR._lookupTable[0x5549 - 0x4145] = VR.UI;
    VR._lookupTable[0x554c - 0x4145] = VR.UL;
    VR._lookupTable[0x554e - 0x4145] = VR.UN;
    VR._lookupTable[0x5552 - 0x4145] = VR.UR;
    VR._lookupTable[0x5553 - 0x4145] = VR.US;
    VR._lookupTable[0x5554 - 0x4145] = VR.UT;
    return true;
  }

  void checkMaxValueLength(int length) {
    if (length > maxLength)
      throw new RangeError("Value Field length too large for VR=$name");
  }
}


// Initialize the VR.lookup table
//TODO what is the best way to do initialization
final bool vrIsInitialized = VR.initializeVR();




