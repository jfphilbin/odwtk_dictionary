// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library vr_constants;

import 'dart:typed_data';
import 'package:dictionary/vr.dart';

//TODO what is the fastest Map implementation for small maps.
//NOTE: This has been moved back into VR.dart
/*
const Map<String, VR> nameToVR =
    const {"AE": VR.AE, "AS": VR.AS, "AT": VR.AT, "BR": VR.BR, "CS": VR.CS, "DA": VR.DA,
           "DS": VR.DS, "DT": VR.DT, "FD": VR.FD, "FL": VR.FL, "IS": VR.IS, "LO": VR.LO,
           "LT": VR.LT, "OB": VR.OB, "OD": VR.OD, "OF": VR.OF, "OW": VR.OW, "PN": VR.PN,
           "SH": VR.SH, "SL": VR.SL, "SQ": VR.SQ, "SS": VR.SS, "ST": VR.ST, "TM": VR.TM,
           "UI": VR.UI, "UL": VR.UL, "UN": VR.UN, "UR": VR.UR, "US": VR.US, "UT": VR.UT};
*/
const Map<String, int> nameToVRIndex =
    const {"Error": 0,
           "AE": 01, "AS": 02, "AT": 03, "BR": 04, "CS": 05, "DA": 06,
           "DS": 07, "DT": 08, "FD": 09, "FL": 10, "IS": 11, "LO": 12,
           "LT": 13, "OB": 14, "OD": 15, "OF": 16, "OW": 17, "PN": 18,
           "SH": 19, "SL": 20, "SQ": 21, "SS": 22, "ST": 23, "TM": 24,
           "UI": 25, "UL": 26, "UN": 27, "UR": 28, "US": 29, "UT": 30};

const List<VR> intToVR =
    const [VR.AE, VR.AS, VR.AT, VR.BR, VR.CS, VR.DA, VR.DS, VR.DT, VR.FD, VR.FL, VR.IS,
           VR.LO, VR.LT, VR.OB, VR.OD, VR.OF, VR.OW, VR.PN, VR.SH, VR.SL, VR.SQ, VR.SS,
           VR.ST, VR.TM, VR.UI, VR.UL, VR.UN, VR.UR, VR.US, VR.UT];


const Uint8List A = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2];
const Uint8List B = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3];
const Uint8List C = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4];
const Uint8List D = const [5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 7];
const Uint8List F = const [0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 9];
const Uint8List I = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10];
const Uint8List L = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 12];
const Uint8List O = const [0, 13, 0, 14, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16];
const Uint8List P = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17];
const Uint8List S = const [0, 0, 0, 0, 0, 0, 0, 18, 0, 0, 0, 19, 0, 0, 0, 0, 20, 0, 21, 22];
const Uint8List T = const [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0];
const Uint8List U = const [0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 25, 0, 26, 0, 0, 0, 27, 28, 29];
const Uint8List empty = const [];

const List<Uint8List> vrL1List =
    const [A, B, C, D, empty, F, empty, empty, I, empty, empty, L, empty, empty,
           O, P, empty, empty, S, T, U, empty, empty, empty, empty];

int lookupVRIndex(int char1, int char2) {
  int i = char1 - 0x41; //ASCII.A
  int j = char2 - 0x41;
  List vrL2List = vrL1List[i];
  if (vrL2List == empty) throw new ArgumentError("Invalid VR 1st char=$i");
  int vrIndex = vrL2List[j];
  if (vrIndex == 0) throw new ArgumentError("Invalid VR 2nd char=$j");
  return vrIndex;
}

const List<VR> indexToVR =
    const [null, VR.AE, VR.AS, VR.AT, VR.BR, VR.CS, VR.DA, VR.DS, VR.DT, VR.FD, VR.FL, VR.IS,
           VR.LO, VR.LT, VR.OB, VR.OD, VR.OF, VR.OW, VR.PN, VR.SH, VR.SL, VR.SQ, VR.SS,
           VR.ST, VR.TM, VR.UI, VR.UL, VR.UN, VR.UR, VR.US, VR.UT];

VR lookupVR(int char1, int char2) => indexToVR[lookupVRIndex(char1, char2)];

class VRidx {
  static const AE = 1;
  static const AS = 2;
  static const AT = 3;
  static const BR = 4;
  static const CS = 5;
  static const DA = 6;
  static const DS = 7;
  static const DT = 8;
  static const FD = 9;
  static const FL = 10;
  static const IS = 11;
  static const LO = 12;
  static const LT = 13;
  static const OB = 14;
  static const OD = 15;
  static const OF = 16;
  static const OW = 17;
  static const PN = 18;
  static const SH = 19;
  static const SL = 20;
  static const SQ = 21;
  static const SS = 22;
  static const ST = 23;
  static const TM = 24;
  static const UI = 25;
  static const UL = 26;
  static const UN = 27;
  static const UR = 28;
  static const US = 29;
  static const UT = 30;
}

//TODO expected that this is slower
const Map<int, VR> codeToVR = const {
    0x4145: VR.AE, 0x4153: VR.AS, 0x4154: VR.AT, 0x4252: VR.BR, 0x4353: VR.CS,
    0x4441: VR.DA, 0x4453: VR.DS, 0x4454: VR.DT, 0x4644: VR.FD, 0x464c: VR.FL,
    0x4953: VR.IS, 0x4c4f: VR.LO, 0x4c54: VR.LT, 0x4f42: VR.OB, 0x4f44: VR.OD,
    0x4f46: VR.OF, 0x4f57: VR.OW, 0x504e: VR.PN, 0x5348: VR.SH, 0x534c: VR.SL,
    0x5351: VR.SQ, 0x5353: VR.SS, 0x5354: VR.ST, 0x544d: VR.TM, 0x5549: VR.UI,
    0x554c: VR.UL, 0x554e: VR.UN, 0x5552: VR.UR, 0x5553: VR.US, 0x5554: VR.UT };

//bool vrIsInitialized = false;

//
/**
 * A lookup table for VR codes.
 *
 * Initializes the VR lookup table.  This function must be called before VR static members
 * can be accessed.
 */
final List<VR> lookupTable = _initializeLookupTable();

VR valueOfVR(int vrCode) {
  VR vr = lookupTable[vrCode - 0x4145];
  if (vr == null) throw new Exception("Bad vrCode = $vrCode");
  return vr;
}

List<VR> _initializeLookupTable() {
  List<VR> lt = new List<VR>(0x5554 - 0x4145 + 1);

  lookupTable[0x4145 - 0x4145] = VR.AE;
  lookupTable[0x4153 - 0x4145] = VR.AS;
  lookupTable[0x4154 - 0x4145] = VR.AT;
  lookupTable[0x4252 - 0x4145] = VR.BR;
  lookupTable[0x4353 - 0x4145] = VR.CS;
  lookupTable[0x4441 - 0x4145] = VR.DA;
  lookupTable[0x4453 - 0x4145] = VR.DS;
  lookupTable[0x4454 - 0x4145] = VR.DT;
  lookupTable[0x4644 - 0x4145] = VR.FD;
  lookupTable[0x464c - 0x4145] = VR.FL;
  lookupTable[0x4953 - 0x4145] = VR.IS;
  lookupTable[0x4c4f - 0x4145] = VR.LO;
  lookupTable[0x4c54 - 0x4145] = VR.LT;
  lookupTable[0x4f42 - 0x4145] = VR.OB;
  lookupTable[0x4f44 - 0x4145] = VR.OD;
  lookupTable[0x4f46 - 0x4145] = VR.OF;
  lookupTable[0x4f57 - 0x4145] = VR.OW;
  lookupTable[0x504e - 0x4145] = VR.PN;
  lookupTable[0x5348 - 0x4145] = VR.SH;
  lookupTable[0x534c - 0x4145] = VR.SL;
  lookupTable[0x5351 - 0x4145] = VR.SQ;
  lookupTable[0x5353 - 0x4145] = VR.SS;
  lookupTable[0x5354 - 0x4145] = VR.ST;
  lookupTable[0x544d - 0x4145] = VR.TM;
  lookupTable[0x5549 - 0x4145] = VR.UI;
  lookupTable[0x554c - 0x4145] = VR.UL;
  lookupTable[0x554e - 0x4145] = VR.UN;
  lookupTable[0x5552 - 0x4145] = VR.UR;
  lookupTable[0x5553 - 0x4145] = VR.US;
  lookupTable[0x5554 - 0x4145] = VR.UT;
  return lookupTable;
}


//TODO ** compare nameToVR and stringToVR **
VR stringToVR(String s) {
  switch (s) {
    case "AE": return VR.AE;
    case "AS": return VR.AS;
    case "AT": return VR.AT;
    case "BR": return VR.BR;
    case "CS": return VR.CS;
    case "DA": return VR.DA;
    case "DS": return VR.DS;
    case "DT": return VR.DT;
    case "FD": return VR.FD;
    case "FL": return VR.FL;
    case "IS": return VR.IS;
    case "LO": return VR.LO;
    case "LT": return VR.LT;
    case "OB": return VR.OB;
    case "OD": return VR.OD;
    case "OF": return VR.OF;
    case "OW": return VR.OW;
    case "PN": return VR.PN;
    case "SH": return VR.SH;
    case "SL": return VR.SL;
    case "SQ": return VR.SQ;
    case "SS": return VR.SS;
    case "ST": return VR.ST;
    case "TM": return VR.TM;
    case "UI": return VR.UI;
    case "UL": return VR.UL;
    case "UN": return VR.UN;
    case "UR": return VR.UR;
    case "US": return VR.US;
    case "UT": return VR.UT;
    default: return null;
  }
}
/*
// Initialize the VR.lookup table
bool initializeVRClass() {
  VR.initializeVR();
  print('VR class initialized');
  vrIsInitialized = true;
  return true;
}
*/
/*
'AE', 0x4145,
'AS', 0x4153,
'AT', 0x4154,
'BR', 0x4252,
'CS', 0x4353,
'DA', 0x4441,
'DS', 0x4453,
'DT', 0x4454,
'FD', 0x4644,
'FL', 0x464c,
'IS', 0x4953,
'LO', 0x4c4f,
'LT', 0x4c54,
'OB', 0x4f42,
'OD', 0x4f42,
'OF', 0x4f46,
'OW', 0x4f57,
'PN', 0x504e,
'SH', 0x5348,
'SL', 0x534c,
'SQ', 0x5351,
'SS', 0x5353,
'ST', 0x5354,
'TM', 0x544d,
'UI', 0x5549,
'UL', 0x554c,
'UN', 0x554e,
'UR', 0x5552,
'US', 0x5553,
'UT', 0x5554,
*/
/*
//TODO figure out what to do with these
'OBOW', 0,  2,  0, -1,    false, false, null,       null, "OBOW");
'USOW', 0,  2,  0, -1,    false, false, null,       null, "OSOW");
'USSS', 0,  2,  0, -1,    false, false, null,       null, "USSS");
'USSSOW', 0,  2,  0, -1,false, false, null,       null, "USSSOW");
'NoVR', 0,  0,  0,  0,    false, false, null,       null, null);
*/
