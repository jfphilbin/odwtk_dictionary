// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library utils;

import 'dart:convert';
import 'package:utilities/utilities.dart';

/**
 * A set of utility procedures for use with the DICOM Dictionary Library.
 */

/**
 * Converts a DICOM [keyword] to the equivalent DICOM name.
 *
 * Given a keyword in camelCase, returns a [String] with a space (' ')
 * inserted before each uppercase letter.
 *
 * Note: This algorithm does not return the exact [name] string, for example
 * some [name]s have appostrophies ("'") in them, but they are not in the [keyword].
 * Also, all dashes ('-') in keywords have been converted to underscores ('_'),
 * because dashes are illegal in Dart identifiers.
 */
String keywordToName(String keyword) {
  List kw = keyword.codeUnits;
  List name = new List();
  name[0] = kw[0];
  for(int i = 0; i < kw.length; i++) {
    int char = kw[i];
    if (isUppercase(char)) name.add($SPACE);
    name.add(char);
  }
  return UTF8.decode(name);
}

